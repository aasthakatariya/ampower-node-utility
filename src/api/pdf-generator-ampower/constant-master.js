const intToRoman = { 1: "i", 2: "ii", 3: "iii", 4: "iv", 5: "v", 6: "vi", 7: "vii", 8: "viii", 9: "ix", 10: "x" }

const professionMaster =
{
    1: "Salaried",
    2: "Self Employed",
    3: "Business",
    4: "Agriculture",
    5: "Professional",
    6: "Student",
    7: "Retired",
    8: "House Wife",
    9: "Unemployed",
    10: "Others",
}
const maritalStatusMaster = {
    4: "Divorced",
    1: "Married",
    5: "Separated",
    2: "Single",
    3: "Widowed"
}

const relationshipMaster =
{
    4: "Father",
    5: "Mother",
    6: "Brother",
    7: "Sister",
    12: "Special Concession Adult",
    13: "Special Concession Child",
    14: "Wife",
    15: "Husband",
    17: "Daughter",
    18: "Father-in-law",
    19: "Mother-in-law",
    20: "Son",
    21: "Grand Father",
    22: "Grand Mother",
    23: "Grand Son",
    24: "Grand Daughter",
    99: "Policy Holder"
}

const stateMaster =
{
    1: "Andaman and Nicobar Islands",
    2: "Andhra Pradesh",
    3: "Arunachal Pradesh",
    4: "Assam",
    5: "Bihar",
    6: "Chandigarh",
    7: "Chhattisgarh",
    8: "Dadra and Nagar Haveli",
    9: "Daman and Diu",
    10: "Delhi",
    11: "Goa",
    12: "Gujarat",
    13: "Haryana",
    14: "Himachal Pradesh",
    15: "Jammu and Kashmir",
    16: "Jharkhand",
    17: "Karnataka",
    18: "Kerala",
    19: "Lakshadweep",
    20: "Madhya Pradesh",
    21: "Maharashtra",
    22: "Manipur",
    23: "Meghalaya",
    24: "Mizoram",
    25: "Nagaland",
    26: "Orissa",
    27: "Puducherry",
    28: "Punjab",
    29: "Rajasthan",
    30: "Sikkim",
    31: "Tamil Nadu",
    32: "Tripura",
    33: "Uttar Pradesh",
    34: "Uttarakhand",
    35: "West Bengal",
    36: "Telangana",

};


const medicalJson = [
    {
        "sub_question_id": 0,
        "question_id": 1,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 1,
        "question_id": 71,
        "value": "MITRAL STENOSIS",
        "disease_id": 1
    },
    {
        "sub_question_id": 2,
        "question_id": 71,
        "value": "AORTIC STENOSIS",
        "disease_id": 2
    },
    {
        "sub_question_id": 3,
        "question_id": 71,
        "value": "TRICUSPID STENOSIS",
        "disease_id": 3
    },
    {
        "sub_question_id": 4,
        "question_id": 71,
        "value": "PULMONARY STENOSIS",
        "disease_id": 4
    },
    {
        "sub_question_id": 5,
        "question_id": 71,
        "value": "MITRAL REGURGITATION (INCOMPETENCE)",
        "disease_id": 5
    },
    {
        "sub_question_id": 6,
        "question_id": 71,
        "value": "AORTIC REGURGITATION (INCOMPETENCE)",
        "disease_id": 6
    },
    {
        "sub_question_id": 7,
        "question_id": 71,
        "value": "TRICUSPID REGURGITATION (INCOMPETENCE)",
        "disease_id": 7
    },
    {
        "sub_question_id": 8,
        "question_id": 71,
        "value": "PULMONARY REGURGITATION (INCOMPETENCE)",
        "disease_id": 8
    },
    {
        "sub_question_id": 9,
        "question_id": 71,
        "value": "ATRIAL SEPTAL DEFECT (ASD)",
        "disease_id": 9
    },
    {
        "sub_question_id": 10,
        "question_id": 71,
        "value": "VENTRICULAR SEPTAL DEFECT (VSD)",
        "disease_id": 10
    },
    {
        "sub_question_id": 11,
        "question_id": 71,
        "value": "PATENT DUCTUS ARTERIOSUS (PDA)",
        "disease_id": 11
    },
    {
        "sub_question_id": 12,
        "question_id": 71,
        "value": "EBSTEINS  ANOMALY",
        "disease_id": 12
    },
    {
        "sub_question_id": 13,
        "question_id": 71,
        "value": "TRANSPOSITION OF GREAT VESSELS (TGV)",
        "disease_id": 13
    },
    {
        "sub_question_id": 14,
        "question_id": 71,
        "value": "COARCTATION OF AORTA",
        "disease_id": 14
    },
    {
        "sub_question_id": 15,
        "question_id": 71,
        "value": "FALLOTS TETRALOGY",
        "disease_id": 15
    },
    {
        "sub_question_id": 16,
        "question_id": 71,
        "value": "HYPERTROPHIC OBSTRUCTIVE CARDIOMYOPATHY (HOCM)",
        "disease_id": 16
    },
    {
        "sub_question_id": 17,
        "question_id": 71,
        "value": "DILATED CARDIOMYOPATHY (DCM)",
        "disease_id": 17
    },
    {
        "sub_question_id": 18,
        "question_id": 71,
        "value": "HEART FAILURE",
        "disease_id": 18
    },
    {
        "sub_question_id": 19,
        "question_id": 71,
        "value": "MYOCARDIAL INFARCTION (HEART ATTACK)",
        "disease_id": 19
    },
    {
        "sub_question_id": 20,
        "question_id": 71,
        "value": "ISCHAEMIC HEART DISEASE (IHD)",
        "disease_id": 20
    },
    {
        "sub_question_id": 21,
        "question_id": 71,
        "value": "PULMONARY THROMBOEMBOLISM",
        "disease_id": 21
    },
    {
        "sub_question_id": 22,
        "question_id": 71,
        "value": "PULMONARY ARTERY HYPERTENSION",
        "disease_id": 22
    },
    {
        "sub_question_id": 23,
        "question_id": 71,
        "value": "ARTERIAL ANEURYSMS",
        "disease_id": 23
    },
    {
        "sub_question_id": 24,
        "question_id": 71,
        "value": "PERIPHERAL VASCULAR DISEASE",
        "disease_id": 24
    },
    {
        "sub_question_id": 25,
        "question_id": 71,
        "value": "OBSTRUCTIVE SLEEP APNOEA",
        "disease_id": 25
    },
    {
        "sub_question_id": 26,
        "question_id": 71,
        "value": "ATRIAL FLUTTER/ FIBRILLATION",
        "disease_id": 26
    },
    {
        "sub_question_id": 27,
        "question_id": 71,
        "value": "SUPRA-VENTRICULAR TACHYCARDIA (SVT OR PSVT)",
        "disease_id": 27
    },
    {
        "sub_question_id": 28,
        "question_id": 71,
        "value": "COMPLETE HEART BLOCK",
        "disease_id": 28
    },
    {
        "sub_question_id": 29,
        "question_id": 71,
        "value": "COR PULMONALE",
        "disease_id": 29
    },
    {
        "sub_question_id": 30,
        "question_id": 71,
        "value": "PERICARDITIS",
        "disease_id": 30
    },
    {
        "sub_question_id": 31,
        "question_id": 71,
        "value": "PERICARDIAL TAMPONADE",
        "disease_id": 31
    },
    {
        "sub_question_id": 32,
        "question_id": 71,
        "value": "HYPERTENSION (HIGH BLOOD PRESSURE)",
        "disease_id": 32
    },
    {
        "sub_question_id": 33,
        "question_id": 71,
        "value": "LEFT VENTRICULAR HYPERTROPHY (LVH)",
        "disease_id": 33
    },
    {
        "sub_question_id": 34,
        "question_id": 71,
        "value": "HYPOTENSION (LOW BLOOD PRESSURE)",
        "disease_id": 34
    },
    {
        "sub_question_id": 35,
        "question_id": 71,
        "value": "DEEP VEIN THROMBOSIS (DVT)",
        "disease_id": 35
    },
    {
        "sub_question_id": 36,
        "question_id": 71,
        "value": "VARICOSE VEINS",
        "disease_id": 36
    },
    {
        "sub_question_id": 37,
        "question_id": 71,
        "value": "LBBB",
        "disease_id": 37
    },
    {
        "sub_question_id": 38,
        "question_id": 71,
        "value": "SITUS INVERSUS",
        "disease_id": 248
    },
    {
        "sub_question_id": 39,
        "question_id": 71,
        "value": "DYSLIPIDEMIA",
        "disease_id": 249
    },
    {
        "sub_question_id": 40,
        "question_id": 71,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 41,
        "question_id": 72,
        "value": "CHRONIC BRONCHITIS",
        "disease_id": 38
    },
    {
        "sub_question_id": 42,
        "question_id": 72,
        "value": "EMPHYSEMA",
        "disease_id": 39
    },
    {
        "sub_question_id": 43,
        "question_id": 72,
        "value": "BRONCHIECTASIS",
        "disease_id": 40
    },
    {
        "sub_question_id": 44,
        "question_id": 72,
        "value": "CYSTIC FIBROSIS",
        "disease_id": 41
    },
    {
        "sub_question_id": 45,
        "question_id": 72,
        "value": "INTERSTITIAL LUNG DISEASE",
        "disease_id": 42
    },
    {
        "sub_question_id": 46,
        "question_id": 72,
        "value": "SARCOIDOSIS",
        "disease_id": 43
    },
    {
        "sub_question_id": 47,
        "question_id": 72,
        "value": "AMYLOIDOSIS",
        "disease_id": 44
    },
    {
        "sub_question_id": 48,
        "question_id": 72,
        "value": "TUBERCULOSIS (TB)",
        "disease_id": 45
    },
    {
        "sub_question_id": 49,
        "question_id": 72,
        "value": "PNEUMONIA",
        "disease_id": 46
    },
    {
        "sub_question_id": 50,
        "question_id": 72,
        "value": "BRONCHIAL ASTHMA",
        "disease_id": 47
    },
    {
        "sub_question_id": 51,
        "question_id": 72,
        "value": "ALLERGIC BRONCHITIS",
        "disease_id": 48
    },
    {
        "sub_question_id": 52,
        "question_id": 72,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 53,
        "question_id": 73,
        "value": "ULCERATIVE COLITIS",
        "disease_id": 49
    },
    {
        "sub_question_id": 54,
        "question_id": 73,
        "value": "CROHNS DISEASE",
        "disease_id": 50
    },
    {
        "sub_question_id": 55,
        "question_id": 73,
        "value": "HEPATITIS B",
        "disease_id": 51
    },
    {
        "sub_question_id": 56,
        "question_id": 73,
        "value": "HEPATITIS C",
        "disease_id": 52
    },
    {
        "sub_question_id": 57,
        "question_id": 73,
        "value": "CIRRHOSIS",
        "disease_id": 53
    },
    {
        "sub_question_id": 58,
        "question_id": 73,
        "value": "ALCOHOLIC LIVER DISEASE",
        "disease_id": 54
    },
    {
        "sub_question_id": 59,
        "question_id": 73,
        "value": "PORTAL HYPERTENSION",
        "disease_id": 55
    },
    {
        "sub_question_id": 60,
        "question_id": 73,
        "value": "CHRONIC PANCREATITIS",
        "disease_id": 56
    },
    {
        "sub_question_id": 61,
        "question_id": 73,
        "value": "PORPHYRIAS",
        "disease_id": 57
    },
    {
        "sub_question_id": 62,
        "question_id": 73,
        "value": "ACHALASIA CARDIA",
        "disease_id": 58
    },
    {
        "sub_question_id": 63,
        "question_id": 73,
        "value": "FATTY LIVER (NASH OR NON ALCOHOLIC STEATO HEPATITIS)",
        "disease_id": 59
    },
    {
        "sub_question_id": 64,
        "question_id": 73,
        "value": "WILSONS DISEASE",
        "disease_id": 60
    },
    {
        "sub_question_id": 65,
        "question_id": 73,
        "value": "CHRONIC HEPATITIS",
        "disease_id": 61
    },
    {
        "sub_question_id": 66,
        "question_id": 73,
        "value": "TYPHOID",
        "disease_id": 62
    },
    {
        "sub_question_id": 67,
        "question_id": 73,
        "value": "HEPATITIS (JAUNDICE)",
        "disease_id": 63
    },
    {
        "sub_question_id": 68,
        "question_id": 73,
        "value": "GASTROENTERITIS",
        "disease_id": 64
    },
    {
        "sub_question_id": 69,
        "question_id": 73,
        "value": "ACID PEPTIC DISEASE (APD)",
        "disease_id": 65
    },
    {
        "sub_question_id": 70,
        "question_id": 73,
        "value": "GASTRO-OESOPHAGEAL REFLUX DISORDER (GERD)",
        "disease_id": 66
    },
    {
        "sub_question_id": 71,
        "question_id": 73,
        "value": "PANCREATITIS",
        "disease_id": 67
    },
    {
        "sub_question_id": 72,
        "question_id": 73,
        "value": "CHOLELITHIASIS (GALL BLADDER STONE)",
        "disease_id": 68
    },
    {
        "sub_question_id": 73,
        "question_id": 73,
        "value": "CHOLECYSTITIS",
        "disease_id": 69
    },
    {
        "sub_question_id": 74,
        "question_id": 73,
        "value": "CHOLEDOCHAL CYST",
        "disease_id": 70
    },
    {
        "sub_question_id": 75,
        "question_id": 73,
        "value": "HAEMORRHOIDS (PILES)",
        "disease_id": 71
    },
    {
        "sub_question_id": 76,
        "question_id": 73,
        "value": "FISSURE IN ANO (ANAL FISSURES)",
        "disease_id": 72
    },
    {
        "sub_question_id": 77,
        "question_id": 73,
        "value": "FISTULA IN ANO",
        "disease_id": 73
    },
    {
        "sub_question_id": 78,
        "question_id": 73,
        "value": "HERNIA",
        "disease_id": 74
    },
    {
        "sub_question_id": 79,
        "question_id": 73,
        "value": "INTUSSUSCEPTION (INTESTINAL OBSTRUCTION)",
        "disease_id": 250
    },
    {
        "sub_question_id": 80,
        "question_id": 73,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 81,
        "question_id": 74,
        "value": "KIDNEY (RENAL) FAILURE",
        "disease_id": 75
    },
    {
        "sub_question_id": 82,
        "question_id": 74,
        "value": "NEPHROTIC  SYNDROME",
        "disease_id": 76
    },
    {
        "sub_question_id": 83,
        "question_id": 74,
        "value": "NEPHRITIC SYNDROME",
        "disease_id": 77
    },
    {
        "sub_question_id": 84,
        "question_id": 74,
        "value": "POLYCYSTIC KIDNEY DISEASE",
        "disease_id": 78
    },
    {
        "sub_question_id": 85,
        "question_id": 74,
        "value": "MEDULLARY CYSTIC KIDNEY",
        "disease_id": 79
    },
    {
        "sub_question_id": 86,
        "question_id": 74,
        "value": "HEMATURIA",
        "disease_id": 80
    },
    {
        "sub_question_id": 87,
        "question_id": 74,
        "value": "RENAL CYST",
        "disease_id": 81
    },
    {
        "sub_question_id": 88,
        "question_id": 74,
        "value": "RENAL AND URETERIC CALCULUS (STONE)",
        "disease_id": 82
    },
    {
        "sub_question_id": 89,
        "question_id": 74,
        "value": "URINARY TRACT INFECTION (UTI)",
        "disease_id": 83
    },
    {
        "sub_question_id": 90,
        "question_id": 74,
        "value": "HYDRONEPHROSIS",
        "disease_id": 84
    },
    {
        "sub_question_id": 91,
        "question_id": 74,
        "value": "BENIGN PROSTATIC HYPERTROPHY (BPH)",
        "disease_id": 85
    },
    {
        "sub_question_id": 92,
        "question_id": 74,
        "value": "ECTOPIC KIDNEY",
        "disease_id": 251
    },
    {
        "sub_question_id": 93,
        "question_id": 74,
        "value": "TYPE I DIABETES MELLITUS",
        "disease_id": 252
    },
    {
        "sub_question_id": 94,
        "question_id": 74,
        "value": "TYPE II DIABETES MELLITUS, NOT ON INSULIN TREATMENT",
        "disease_id": 253
    },
    {
        "sub_question_id": 95,
        "question_id": 74,
        "value": "TYPE II DIABETES MELLITUS, ON INSULIN TREATMENT",
        "disease_id": 254
    },
    {
        "sub_question_id": 96,
        "question_id": 74,
        "value": "GESTATIONAL DIABETES",
        "disease_id": 255
    },
    {
        "sub_question_id": 97,
        "question_id": 74,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 98,
        "question_id": 75,
        "value": "EPILEPSY (SEIZURES OR FITS)",
        "disease_id": 86
    },
    {
        "sub_question_id": 99,
        "question_id": 75,
        "value": "STROKE (BRAIN HEMORRHAGE OR CEREBRO-VASCULAR ACCIDENT)",
        "disease_id": 87
    },
    {
        "sub_question_id": 100,
        "question_id": 75,
        "value": "PARALYSIS",
        "disease_id": 88
    },
    {
        "sub_question_id": 101,
        "question_id": 75,
        "value": "ALZHEIMERS DISEASE",
        "disease_id": 89
    },
    {
        "sub_question_id": 102,
        "question_id": 75,
        "value": "PARKINSONS DISEASE",
        "disease_id": 90
    },
    {
        "sub_question_id": 103,
        "question_id": 75,
        "value": "MOTOR NEURON DISORDERS",
        "disease_id": 91
    },
    {
        "sub_question_id": 104,
        "question_id": 75,
        "value": "MULTIPLE SCLEROSIS",
        "disease_id": 92
    },
    {
        "sub_question_id": 105,
        "question_id": 75,
        "value": "MYASTHENIA GRAVIS",
        "disease_id": 93
    },
    {
        "sub_question_id": 106,
        "question_id": 75,
        "value": "TRANSIENT ISCHEMIC ATTACK (TIA)",
        "disease_id": 94
    },
    {
        "sub_question_id": 107,
        "question_id": 75,
        "value": "CEREBRAL PALSY",
        "disease_id": 95
    },
    {
        "sub_question_id": 108,
        "question_id": 75,
        "value": "MENTAL RETARDATION",
        "disease_id": 96
    },
    {
        "sub_question_id": 109,
        "question_id": 75,
        "value": "MIGRAINE",
        "disease_id": 97
    },
    {
        "sub_question_id": 110,
        "question_id": 75,
        "value": "MENINGITIS",
        "disease_id": 98
    },
    {
        "sub_question_id": 111,
        "question_id": 75,
        "value": "MUSCLE WASTING/MUSCLE WEAKNESS",
        "disease_id": 260
    },
    {
        "sub_question_id": 112,
        "question_id": 75,
        "value": "NUMBNESS/BURNING SENSATION",
        "disease_id": 261
    },
    {
        "sub_question_id": 113,
        "question_id": 75,
        "value": "LOSS OF SENSATION",
        "disease_id": 262
    },
    {
        "sub_question_id": 114,
        "question_id": 75,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 115,
        "question_id": 76,
        "value": "DIABETES",
        "disease_id": 99
    },
    {
        "sub_question_id": 116,
        "question_id": 76,
        "value": "IMPAIRED GLUCOSE TOLERANCE (IGT)",
        "disease_id": 100
    },
    {
        "sub_question_id": 117,
        "question_id": 76,
        "value": "IMPAIRED FASTING GLUCOSE (IFG)",
        "disease_id": 101
    },
    {
        "sub_question_id": 118,
        "question_id": 76,
        "value": "HYPOTHYROIDISM",
        "disease_id": 102
    },
    {
        "sub_question_id": 119,
        "question_id": 76,
        "value": "HYPERTHYROIDISM",
        "disease_id": 103
    },
    {
        "sub_question_id": 120,
        "question_id": 76,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 121,
        "question_id": 77,
        "value": "LEUKEMIA",
        "disease_id": 104
    },
    {
        "sub_question_id": 122,
        "question_id": 77,
        "value": "ADRENAL CANCER",
        "disease_id": 105
    },
    {
        "sub_question_id": 123,
        "question_id": 77,
        "value": "KAPOSI SARCOMA",
        "disease_id": 106
    },
    {
        "sub_question_id": 124,
        "question_id": 77,
        "value": "LYMPHOMA",
        "disease_id": 107
    },
    {
        "sub_question_id": 125,
        "question_id": 77,
        "value": "COLON CANCER",
        "disease_id": 108
    },
    {
        "sub_question_id": 126,
        "question_id": 77,
        "value": "ANO-RECTAL CANCER",
        "disease_id": 109
    },
    {
        "sub_question_id": 127,
        "question_id": 77,
        "value": "ASTROCYTOMA",
        "disease_id": 110
    },
    {
        "sub_question_id": 128,
        "question_id": 77,
        "value": "BASAL CELL CARCINOMA",
        "disease_id": 111
    },
    {
        "sub_question_id": 129,
        "question_id": 77,
        "value": "MELANOMA",
        "disease_id": 112
    },
    {
        "sub_question_id": 130,
        "question_id": 77,
        "value": "CHOLANGIOCARCINOMA",
        "disease_id": 113
    },
    {
        "sub_question_id": 131,
        "question_id": 77,
        "value": "BLADDER CANCER",
        "disease_id": 114
    },
    {
        "sub_question_id": 132,
        "question_id": 77,
        "value": "EWINGS SARCOMA",
        "disease_id": 115
    },
    {
        "sub_question_id": 133,
        "question_id": 77,
        "value": "OSTEOSARCOMA",
        "disease_id": 116
    },
    {
        "sub_question_id": 134,
        "question_id": 77,
        "value": "GLIOMA",
        "disease_id": 117
    },
    {
        "sub_question_id": 135,
        "question_id": 77,
        "value": "CRANIOPHARYNGIOMA",
        "disease_id": 118
    },
    {
        "sub_question_id": 136,
        "question_id": 77,
        "value": "BREAST CANCER",
        "disease_id": 119
    },
    {
        "sub_question_id": 137,
        "question_id": 77,
        "value": "CARCINOID",
        "disease_id": 120
    },
    {
        "sub_question_id": 138,
        "question_id": 77,
        "value": "GERM CELL TUMOR",
        "disease_id": 121
    },
    {
        "sub_question_id": 139,
        "question_id": 77,
        "value": "CERVICAL CANCER",
        "disease_id": 122
    },
    {
        "sub_question_id": 140,
        "question_id": 77,
        "value": "CARCINOMA IN SITU (CIS)",
        "disease_id": 123
    },
    {
        "sub_question_id": 141,
        "question_id": 77,
        "value": "ENDOMETRIAL CANCER",
        "disease_id": 124
    },
    {
        "sub_question_id": 142,
        "question_id": 77,
        "value": "ESOPHAGEAL CANCER",
        "disease_id": 125
    },
    {
        "sub_question_id": 143,
        "question_id": 77,
        "value": "RETINOBLASTOMA",
        "disease_id": 126
    },
    {
        "sub_question_id": 144,
        "question_id": 77,
        "value": "GASTRIC (STOMACH) CANCER",
        "disease_id": 127
    },
    {
        "sub_question_id": 145,
        "question_id": 77,
        "value": "LIVER CANCER",
        "disease_id": 128
    },
    {
        "sub_question_id": 146,
        "question_id": 77,
        "value": "ORAL (OROPHARYNGEAL) CANCER",
        "disease_id": 129
    },
    {
        "sub_question_id": 147,
        "question_id": 77,
        "value": "RENAL CANCER",
        "disease_id": 130
    },
    {
        "sub_question_id": 148,
        "question_id": 77,
        "value": "WILMS TUMOR",
        "disease_id": 131
    },
    {
        "sub_question_id": 149,
        "question_id": 77,
        "value": "LUNG CANCER",
        "disease_id": 132
    },
    {
        "sub_question_id": 150,
        "question_id": 77,
        "value": "METASTATIC CANCER",
        "disease_id": 133
    },
    {
        "sub_question_id": 151,
        "question_id": 77,
        "value": "MULTIPLE MYELOMA",
        "disease_id": 134
    },
    {
        "sub_question_id": 152,
        "question_id": 77,
        "value": "NEUROBLASTOMA",
        "disease_id": 135
    },
    {
        "sub_question_id": 153,
        "question_id": 77,
        "value": "OVARIAN CANCER",
        "disease_id": 136
    },
    {
        "sub_question_id": 154,
        "question_id": 77,
        "value": "PANCREATIC CANCER",
        "disease_id": 137
    },
    {
        "sub_question_id": 155,
        "question_id": 77,
        "value": "PARATHYROID CANCER",
        "disease_id": 138
    },
    {
        "sub_question_id": 156,
        "question_id": 77,
        "value": "PHEOCHROMOCYTOMA",
        "disease_id": 139
    },
    {
        "sub_question_id": 157,
        "question_id": 77,
        "value": "PITUITARY TUMOR",
        "disease_id": 140
    },
    {
        "sub_question_id": 158,
        "question_id": 77,
        "value": "PROSTATE CANCER",
        "disease_id": 141
    },
    {
        "sub_question_id": 159,
        "question_id": 77,
        "value": "SQUAMOUS CELL CARCINOMA",
        "disease_id": 142
    },
    {
        "sub_question_id": 160,
        "question_id": 77,
        "value": "TESTICULAR CANCER",
        "disease_id": 143
    },
    {
        "sub_question_id": 161,
        "question_id": 77,
        "value": "THROAT CANCER",
        "disease_id": 144
    },
    {
        "sub_question_id": 162,
        "question_id": 77,
        "value": "THYROID CANCER",
        "disease_id": 145
    },
    {
        "sub_question_id": 163,
        "question_id": 77,
        "value": "UTERINE CANCER",
        "disease_id": 146
    },
    {
        "sub_question_id": 164,
        "question_id": 77,
        "value": "VULVO-VAGINAL CANCER",
        "disease_id": 147
    },
    {
        "sub_question_id": 165,
        "question_id": 77,
        "value": "MULTIPLE MYELOMA",
        "disease_id": 148
    },
    {
        "sub_question_id": 166,
        "question_id": 77,
        "value": "PLEOMORPHIC ADENOMA",
        "disease_id": 239
    },
    {
        "sub_question_id": 167,
        "question_id": 77,
        "value": "DERMATOFIBROSARCOMA PROTUBERANS",
        "disease_id": 240
    },
    {
        "sub_question_id": 168,
        "question_id": 77,
        "value": "NONHEALING ULCER (DIABETIC FOOT)",
        "disease_id": 241
    },
    {
        "sub_question_id": 169,
        "question_id": 77,
        "value": "RECURRENT SKIN INFECTIONS (CARBUNCLE, FURUNCLE, FUNGAL INFECTION, ETC)",
        "disease_id": 242
    },
    {
        "sub_question_id": 170,
        "question_id": 77,
        "value": "INGROWN TOE NAILS/ CORN/ CALLUS",
        "disease_id": 243
    },
    {
        "sub_question_id": 171,
        "question_id": 77,
        "value": "GANGRENE",
        "disease_id": 244
    },
    {
        "sub_question_id": 172,
        "question_id": 77,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 173,
        "question_id": 78,
        "value": "RHEUMATOID ARTHRITIS",
        "disease_id": 149
    },
    {
        "sub_question_id": 174,
        "question_id": 78,
        "value": "MUSCULAR DYSTROPHIES",
        "disease_id": 150
    },
    {
        "sub_question_id": 175,
        "question_id": 78,
        "value": "ANKYLOSING SPONDYLOSIS",
        "disease_id": 151
    },
    {
        "sub_question_id": 176,
        "question_id": 78,
        "value": "DISC PROLAPSE (PIVD OR SPONDYLOSIS)",
        "disease_id": 152
    },
    {
        "sub_question_id": 177,
        "question_id": 78,
        "value": "POLIO",
        "disease_id": 153
    },
    {
        "sub_question_id": 178,
        "question_id": 78,
        "value": "OSTEOPOROSIS",
        "disease_id": 154
    },
    {
        "sub_question_id": 179,
        "question_id": 78,
        "value": "FRACTURE",
        "disease_id": 155
    },
    {
        "sub_question_id": 180,
        "question_id": 78,
        "value": "SEPTIC ARTHRITIS",
        "disease_id": 156
    },
    {
        "sub_question_id": 181,
        "question_id": 78,
        "value": "OSTEOARTHRITIS (OA)",
        "disease_id": 157
    },
    {
        "sub_question_id": 182,
        "question_id": 78,
        "value": "VERTEBRAL CANAL STENOSIS",
        "disease_id": 158
    },
    {
        "sub_question_id": 183,
        "question_id": 78,
        "value": "NONHEALING ULCER (DIABETIC FOOT)",
        "disease_id": 241
    },
    {
        "sub_question_id": 184,
        "question_id": 78,
        "value": "RECURRENT SKIN INFECTIONS (CARBUNCLE, FURUNCLE, FUNGAL INFECTION, ETC)",
        "disease_id": 242
    },
    {
        "sub_question_id": 185,
        "question_id": 78,
        "value": "INGROWN TOE NAILS/ CORN/ CALLUS",
        "disease_id": 243
    },
    {
        "sub_question_id": 186,
        "question_id": 78,
        "value": "GANGRENE",
        "disease_id": 244
    },
    {
        "sub_question_id": 187,
        "question_id": 78,
        "value": "AVASCULAR NECROSIS (AVN)",
        "disease_id": 256
    },
    {
        "sub_question_id": 188,
        "question_id": 78,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 189,
        "question_id": 79,
        "value": "GLAUCOMA",
        "disease_id": 159
    },
    {
        "sub_question_id": 190,
        "question_id": 79,
        "value": "ABSCESS",
        "disease_id": 160
    },
    {
        "sub_question_id": 191,
        "question_id": 79,
        "value": "CONJUNCTIVITIS",
        "disease_id": 161
    },
    {
        "sub_question_id": 192,
        "question_id": 79,
        "value": "VERTIGO",
        "disease_id": 162
    },
    {
        "sub_question_id": 193,
        "question_id": 79,
        "value": "ALLERGY (EXCEPT ALLERGIC BRONCHITIS)",
        "disease_id": 163
    },
    {
        "sub_question_id": 194,
        "question_id": 79,
        "value": "CHOLESTEATOMA",
        "disease_id": 164
    },
    {
        "sub_question_id": 195,
        "question_id": 79,
        "value": "SINUSITIS",
        "disease_id": 165
    },
    {
        "sub_question_id": 196,
        "question_id": 79,
        "value": "DEVIATED NASAL SEPTUM (DNS)",
        "disease_id": 166
    },
    {
        "sub_question_id": 197,
        "question_id": 79,
        "value": "PSORIASIS",
        "disease_id": 167
    },
    {
        "sub_question_id": 198,
        "question_id": 79,
        "value": "RIGHT EYE CATARACT",
        "disease_id": 168
    },
    {
        "sub_question_id": 199,
        "question_id": 79,
        "value": "LEFT EYE CATARACT",
        "disease_id": 169
    },
    {
        "sub_question_id": 200,
        "question_id": 79,
        "value": "ACUTE OTITIS MEDIA (ASOM)",
        "disease_id": 170
    },
    {
        "sub_question_id": 201,
        "question_id": 79,
        "value": "CHRONIC OTITIS MEDIA (CSOM)",
        "disease_id": 171
    },
    {
        "sub_question_id": 202,
        "question_id": 79,
        "value": "OTOSCLEROSIS",
        "disease_id": 172
    },
    {
        "sub_question_id": 203,
        "question_id": 79,
        "value": "OTITIS EXTERNA",
        "disease_id": 173
    },
    {
        "sub_question_id": 204,
        "question_id": 79,
        "value": "DEAFNESS",
        "disease_id": 257
    },
    {
        "sub_question_id": 205,
        "question_id": 79,
        "value": "RETINOPATHY",
        "disease_id": 258
    },
    {
        "sub_question_id": 206,
        "question_id": 79,
        "value": "BLURRED VISION",
        "disease_id": 259
    },
    {
        "sub_question_id": 207,
        "question_id": 79,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 208,
        "question_id": 80,
        "value": "HIV/AIDS",
        "disease_id": 174
    },
    {
        "sub_question_id": 209,
        "question_id": 80,
        "value": "DOWNS SYNDROME",
        "disease_id": 175
    },
    {
        "sub_question_id": 210,
        "question_id": 80,
        "value": "KLINEFELTERS SYNDROME",
        "disease_id": 176
    },
    {
        "sub_question_id": 211,
        "question_id": 80,
        "value": "TURNERS SYNDROME",
        "disease_id": 177
    },
    {
        "sub_question_id": 212,
        "question_id": 80,
        "value": "SYSTEMIC LUPUS ERYTHEMATOSUS (SLE)",
        "disease_id": 178
    },
    {
        "sub_question_id": 213,
        "question_id": 80,
        "value": "SYSTEMIC SCLEROSIS",
        "disease_id": 179
    },
    {
        "sub_question_id": 214,
        "question_id": 80,
        "value": "SCLERODERMA",
        "disease_id": 180
    },
    {
        "sub_question_id": 215,
        "question_id": 80,
        "value": "SYPHILLIS",
        "disease_id": 181
    },
    {
        "sub_question_id": 216,
        "question_id": 80,
        "value": "GONORRHEA",
        "disease_id": 182
    },
    {
        "sub_question_id": 217,
        "question_id": 80,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 218,
        "question_id": 81,
        "value": "THALASSEMIA MAJOR",
        "disease_id": 183
    },
    {
        "sub_question_id": 219,
        "question_id": 81,
        "value": "APLASTIC ANEMIA",
        "disease_id": 184
    },
    {
        "sub_question_id": 220,
        "question_id": 81,
        "value": "POLYCYTHEMIA VERA",
        "disease_id": 185
    },
    {
        "sub_question_id": 221,
        "question_id": 81,
        "value": "IDIOPATHIC THROMBOCYTOPENIC PURPURA (ITP)",
        "disease_id": 186
    },
    {
        "sub_question_id": 222,
        "question_id": 81,
        "value": "HEMOPHILIA",
        "disease_id": 187
    },
    {
        "sub_question_id": 223,
        "question_id": 81,
        "value": "SICKLE CELL DISEASE",
        "disease_id": 188
    },
    {
        "sub_question_id": 224,
        "question_id": 81,
        "value": "THROMBOCYTOPENIA",
        "disease_id": 189
    },
    {
        "sub_question_id": 225,
        "question_id": 81,
        "value": "DENGUE",
        "disease_id": 238
    },
    {
        "sub_question_id": 226,
        "question_id": 81,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 227,
        "question_id": 82,
        "value": "SCHIZOPHRENIA",
        "disease_id": 190
    },
    {
        "sub_question_id": 228,
        "question_id": 82,
        "value": "DEPRESSION",
        "disease_id": 191
    },
    {
        "sub_question_id": 229,
        "question_id": 82,
        "value": "ANXIETY",
        "disease_id": 192
    },
    {
        "sub_question_id": 230,
        "question_id": 82,
        "value": "BIPOLAR DISORDER",
        "disease_id": 193
    },
    {
        "sub_question_id": 231,
        "question_id": 82,
        "value": "OBSESSIVE COMPULSIVE DISORDER (OCD)",
        "disease_id": 194
    },
    {
        "sub_question_id": 232,
        "question_id": 82,
        "value": "ANOREXIA NERVOSA",
        "disease_id": 195
    },
    {
        "sub_question_id": 233,
        "question_id": 82,
        "value": "BULIMIA NERVOSA",
        "disease_id": 196
    },
    {
        "sub_question_id": 234,
        "question_id": 82,
        "value": "INSOMNIA",
        "disease_id": 197
    },
    {
        "sub_question_id": 235,
        "question_id": 82,
        "value": "MANIA",
        "disease_id": 198
    },
    {
        "sub_question_id": 236,
        "question_id": 82,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 237,
        "question_id": 83,
        "value": "POLYCYSTIC OVARIAN DISEASE",
        "disease_id": 199
    },
    {
        "sub_question_id": 238,
        "question_id": 83,
        "value": "VARICOCELE",
        "disease_id": 200
    },
    {
        "sub_question_id": 239,
        "question_id": 83,
        "value": "PELVIC INFLAMMATORY DISORDER (PID)",
        "disease_id": 201
    },
    {
        "sub_question_id": 240,
        "question_id": 83,
        "value": "FIBROID UTERUS",
        "disease_id": 202
    },
    {
        "sub_question_id": 241,
        "question_id": 83,
        "value": "OVARIAN CYST",
        "disease_id": 203
    },
    {
        "sub_question_id": 242,
        "question_id": 83,
        "value": "PROLAPSE UTERUS",
        "disease_id": 204
    },
    {
        "sub_question_id": 243,
        "question_id": 83,
        "value": "FIBROADENOMA BREAST",
        "disease_id": 205
    },
    {
        "sub_question_id": 244,
        "question_id": 83,
        "value": "HYDROCELE",
        "disease_id": 206
    },
    {
        "sub_question_id": 245,
        "question_id": 83,
        "value": "OVARIAN/UTERINE MASS",
        "disease_id": 245
    },
    {
        "sub_question_id": 246,
        "question_id": 83,
        "value": "ENDOMETRIOSIS",
        "disease_id": 246
    },
    {
        "sub_question_id": 247,
        "question_id": 83,
        "value": "FOURNIER GANGRENE",
        "disease_id": 247
    },
    {
        "sub_question_id": 248,
        "question_id": 83,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 249,
        "question_id": 87,
        "value": "BARIATRIC SURGERY",
        "disease_id": 207
    },
    {
        "sub_question_id": 250,
        "question_id": 87,
        "value": "HYSTERECTOMY",
        "disease_id": 208
    },
    {
        "sub_question_id": 251,
        "question_id": 87,
        "value": "MYOMECTOMY",
        "disease_id": 209
    },
    {
        "sub_question_id": 252,
        "question_id": 87,
        "value": "TYMPANOPLASTY",
        "disease_id": 210
    },
    {
        "sub_question_id": 253,
        "question_id": 87,
        "value": "MYRINGOPLASTY",
        "disease_id": 211
    },
    {
        "sub_question_id": 254,
        "question_id": 87,
        "value": "MASTOIDECTOMY",
        "disease_id": 212
    },
    {
        "sub_question_id": 255,
        "question_id": 87,
        "value": "CORONORY ARTERIAL BYPASS GRAFTING (CABG OR HEART BYPASS)",
        "disease_id": 213
    },
    {
        "sub_question_id": 256,
        "question_id": 87,
        "value": "PERCUTANEOUS TRANSLUMINAL CORONARY ANGIOPLASTY (PTCA OR CORNOARY ANGIOPLASTY)",
        "disease_id": 214
    },
    {
        "sub_question_id": 257,
        "question_id": 87,
        "value": "CARDIAC PACEMAKER",
        "disease_id": 215
    },
    {
        "sub_question_id": 258,
        "question_id": 87,
        "value": "RENAL TRANSPLANTATION",
        "disease_id": 216
    },
    {
        "sub_question_id": 259,
        "question_id": 87,
        "value": "HEPATIC TRANSPLANTATION (LIVER)",
        "disease_id": 217
    },
    {
        "sub_question_id": 260,
        "question_id": 87,
        "value": "HEART TRANSPLANTATION",
        "disease_id": 218
    },
    {
        "sub_question_id": 261,
        "question_id": 87,
        "value": "LUNG TRANSPLANTATION",
        "disease_id": 219
    },
    {
        "sub_question_id": 262,
        "question_id": 87,
        "value": "PANCREATIC TRANSPLANTATION",
        "disease_id": 220
    },
    {
        "sub_question_id": 263,
        "question_id": 87,
        "value": "BONE MARROW TRANSPLANTATION",
        "disease_id": 221
    },
    {
        "sub_question_id": 264,
        "question_id": 87,
        "value": "LEFT KNEE REPLACEMENT",
        "disease_id": 222
    },
    {
        "sub_question_id": 265,
        "question_id": 87,
        "value": "RIGHT KNEE REPLACEMENT",
        "disease_id": 223
    },
    {
        "sub_question_id": 266,
        "question_id": 87,
        "value": "BILATERAL (BOTH) KNEE REPLACEMENT",
        "disease_id": 224
    },
    {
        "sub_question_id": 267,
        "question_id": 87,
        "value": "LEFT HIP REPLACEMENT",
        "disease_id": 225
    },
    {
        "sub_question_id": 268,
        "question_id": 87,
        "value": "RIGHT HIP REPLACEMENT",
        "disease_id": 226
    },
    {
        "sub_question_id": 269,
        "question_id": 87,
        "value": "BILATERAL (BOTH) HIP REPLACEMENT",
        "disease_id": 227
    },
    {
        "sub_question_id": 270,
        "question_id": 87,
        "value": "LEFT SHOULDER REPLACEMENT",
        "disease_id": 228
    },
    {
        "sub_question_id": 271,
        "question_id": 87,
        "value": "RIGHT SHOULDER REPLACEMENT",
        "disease_id": 229
    },
    {
        "sub_question_id": 272,
        "question_id": 87,
        "value": "BILATERAL (BOTH) SHOULDER REPLACEMENT",
        "disease_id": 230
    },
    {
        "sub_question_id": 273,
        "question_id": 87,
        "value": "BARIATRIC SURGERY",
        "disease_id": 231
    },
    {
        "sub_question_id": 274,
        "question_id": 87,
        "value": "TONSILLECTOMY",
        "disease_id": 232
    },
    {
        "sub_question_id": 275,
        "question_id": 87,
        "value": "CRANIOTOMY (BURR HOLE SURGERY)",
        "disease_id": 233
    },
    {
        "sub_question_id": 276,
        "question_id": 87,
        "value": "THYROIDECTOMY",
        "disease_id": 234
    },
    {
        "sub_question_id": 277,
        "question_id": 87,
        "value": "PAROTIDECTOMY",
        "disease_id": 235
    },
    {
        "sub_question_id": 278,
        "question_id": 87,
        "value": "COLONOSCOPY",
        "disease_id": 236
    },
    {
        "sub_question_id": 279,
        "question_id": 87,
        "value": "ORCHIDECTOMY (ORCHIECTOMY)",
        "disease_id": 237
    },
    {
        "sub_question_id": 280,
        "question_id": 87,
        "value": "OTHERS",
        "disease_id": 999
    },
    {
        "sub_question_id": 281,
        "question_id": 25,
        "value": "Coma",
        "disease_id": 263
    },
    {
        "sub_question_id": 282,
        "question_id": 25,
        "value": "Unconsciousness",
        "disease_id": 264
    },
    {
        "sub_question_id": 283,
        "question_id": 25,
        "value": "Stroke",
        "disease_id": 265
    },
    {
        "sub_question_id": 284,
        "question_id": 25,
        "value": "Paralysis",
        "disease_id": 266
    },
    {
        "sub_question_id": 285,
        "question_id": 25,
        "value": "Seizures/Epilepsy",
        "disease_id": 267
    },
    {
        "sub_question_id": 287,
        "question_id": 25,
        "value": "Alzheimer's disease",
        "disease_id": 268
    },
    {
        "sub_question_id": 288,
        "question_id": 25,
        "value": "Parkinsonism",
        "disease_id": 269
    },
    {
        "sub_question_id": 289,
        "question_id": 25,
        "value": "OTHERS",
        "disease_id": 270
    },
    {
        "sub_question_id": 290,
        "question_id": 26,
        "value": "Feeble/Absent pulse",
        "disease_id": 271
    },
    {
        "sub_question_id": 291,
        "question_id": 26,
        "value": "Chest pain/Angina",
        "disease_id": 272
    },
    {
        "sub_question_id": 292,
        "question_id": 26,
        "value": "Heart attack",
        "disease_id": 273
    },
    {
        "sub_question_id": 293,
        "question_id": 26,
        "value": "Palpitation",
        "disease_id": 274
    },
    {
        "sub_question_id": 294,
        "question_id": 26,
        "value": "Heart bypass surgery",
        "disease_id": 275
    },
    {
        "sub_question_id": 295,
        "question_id": 26,
        "value": "Heart angioplasty",
        "disease_id": 276
    },
    {
        "sub_question_id": 296,
        "question_id": 26,
        "value": "Heart failure",
        "disease_id": 277
    },
    {
        "sub_question_id": 297,
        "question_id": 26,
        "value": "OTHERS",
        "disease_id": 278
    },
    {
        "sub_question_id": 298,
        "question_id": 27,
        "value": "Asthma",
        "disease_id": 279
    },
    {
        "sub_question_id": 299,
        "question_id": 27,
        "value": "Bronchitis",
        "disease_id": 280
    },
    {
        "sub_question_id": 300,
        "question_id": 27,
        "value": "Pneumonia",
        "disease_id": 281
    },
    {
        "sub_question_id": 301,
        "question_id": 27,
        "value": "Tuberculosis",
        "disease_id": 282
    },
    {
        "sub_question_id": 302,
        "question_id": 27,
        "value": "OTHERS",
        "disease_id": 283
    },
    {
        "sub_question_id": 303,
        "question_id": 28,
        "value": "Hepatitis B/C",
        "disease_id": 284
    },
    {
        "sub_question_id": 304,
        "question_id": 28,
        "value": "Cirrhosis",
        "disease_id": 285
    },
    {
        "sub_question_id": 305,
        "question_id": 28,
        "value": "Inflammatory bowel disease",
        "disease_id": 286
    },
    {
        "sub_question_id": 306,
        "question_id": 28,
        "value": "Pancreatitis",
        "disease_id": 287
    },
    {
        "sub_question_id": 307,
        "question_id": 28,
        "value": "Alcoholic liver disease",
        "disease_id": 288
    },
    {
        "sub_question_id": 308,
        "question_id": 28,
        "value": "OTHERS",
        "disease_id": 289
    },
    {
        "sub_question_id": 309,
        "question_id": 29,
        "value": "Arthritis",
        "disease_id": 290
    },
    {
        "sub_question_id": 310,
        "question_id": 29,
        "value": "Spondylosis",
        "disease_id": 291
    },
    {
        "sub_question_id": 311,
        "question_id": 29,
        "value": "OTHERS",
        "disease_id": 292
    },
    {
        "sub_question_id": 312,
        "question_id": 30,
        "value": "Retinopathy",
        "disease_id": 293
    },
    {
        "sub_question_id": 313,
        "question_id": 30,
        "value": "Cataract",
        "disease_id": 294
    },
    {
        "sub_question_id": 314,
        "question_id": 30,
        "value": "Glaucoma",
        "disease_id": 295
    },
    {
        "sub_question_id": 315,
        "question_id": 30,
        "value": "Sinusitis",
        "disease_id": 296
    },
    {
        "sub_question_id": 316,
        "question_id": 30,
        "value": "OTHERS",
        "disease_id": 297
    },
    {
        "sub_question_id": 317,
        "question_id": 31,
        "value": "Numbness",
        "disease_id": 298
    },
    {
        "sub_question_id": 318,
        "question_id": 31,
        "value": "Tingling",
        "disease_id": 299
    },
    {
        "sub_question_id": 319,
        "question_id": 31,
        "value": "Painful sensation",
        "disease_id": 300
    },
    {
        "sub_question_id": 320,
        "question_id": 31,
        "value": "Ulcer in the limbs",
        "disease_id": 301
    },
    {
        "sub_question_id": 321,
        "question_id": 32,
        "value": "Kidney (Protein or albumin in urine)",
        "disease_id": 302
    },
    {
        "sub_question_id": 322,
        "question_id": 32,
        "value": "Kidney and urinary tract stone",
        "disease_id": 303
    },
    {
        "sub_question_id": 323,
        "question_id": 32,
        "value": "Kidney failure",
        "disease_id": 304
    },
    {
        "sub_question_id": 324,
        "question_id": 32,
        "value": "Prostate enlargement",
        "disease_id": 305
    },
    {
        "sub_question_id": 325,
        "question_id": 32,
        "value": "OTHERS",
        "disease_id": 306
    },
    {
        "sub_question_id": 327,
        "question_id": 33,
        "value": "Hypothyroidism",
        "disease_id": 308
    },
    {
        "sub_question_id": 328,
        "question_id": 33,
        "value": "Hyperthyroidism",
        "disease_id": 309
    },
    {
        "sub_question_id": 329,
        "question_id": 33,
        "value": "OTHERS",
        "disease_id": 310
    },
    {
        "sub_question_id": 330,
        "question_id": 34,
        "value": "Fibroid",
        "disease_id": 311
    },
    {
        "sub_question_id": 331,
        "question_id": 34,
        "value": "Fibroadenoma",
        "disease_id": 312
    },
    {
        "sub_question_id": 332,
        "question_id": 34,
        "value": "Lymphoma",
        "disease_id": 313
    },
    {
        "sub_question_id": 333,
        "question_id": 34,
        "value": "Cancer",
        "disease_id": 314
    },
    {
        "sub_question_id": 336,
        "question_id": 34,
        "value": "others",
        "disease_id": 317
    },
    {
        "sub_question_id": 337,
        "question_id": 35,
        "value": "HIV/AIDS",
        "disease_id": 318
    },
    {
        "sub_question_id": 338,
        "question_id": 35,
        "value": "Sexually transmitted disease",
        "disease_id": 319
    },
    {
        "sub_question_id": 339,
        "question_id": 35,
        "value": "OTHERS",
        "disease_id": 320
    },
    {
        "sub_question_id": 340,
        "question_id": 36,
        "value": "Leukemia",
        "disease_id": 321
    },
    {
        "sub_question_id": 341,
        "question_id": 36,
        "value": "Anemia",
        "disease_id": 322
    },
    {
        "sub_question_id": 342,
        "question_id": 36,
        "value": "Thallasemia",
        "disease_id": 323
    },
    {
        "sub_question_id": 343,
        "question_id": 36,
        "value": "Hemophilia",
        "disease_id": 324
    },
    {
        "sub_question_id": 345,
        "question_id": 36,
        "value": "OTHERS",
        "disease_id": 326
    },
    {
        "sub_question_id": 346,
        "question_id": 37,
        "value": "Depression",
        "disease_id": 327
    },
    {
        "sub_question_id": 347,
        "question_id": 37,
        "value": "Bipolar disorder",
        "disease_id": 328
    },
    {
        "sub_question_id": 348,
        "question_id": 37,
        "value": "OTHERS",
        "disease_id": 329
    },
    {
        "sub_question_id": 349,
        "question_id": 38,
        "value": "Psoriasis",
        "disease_id": 330
    },
    {
        "sub_question_id": 350,
        "question_id": 38,
        "value": "OTHERS",
        "disease_id": 331
    },
    {
        "sub_question_id": 351,
        "question_id": 39,
        "value": "Rhematoid arthritis",
        "disease_id": 332
    },
    {
        "sub_question_id": 352,
        "question_id": 39,
        "value": "Systemic sclerosis",
        "disease_id": 333
    },
    {
        "sub_question_id": 353,
        "question_id": 39,
        "value": "OTHERS",
        "disease_id": 334
    },
    {
        "sub_question_id": 354,
        "question_id": 41,
        "value": "OTHERS",
        "disease_id": 335
    },
    {
        "sub_question_id": 1001,
        "question_id": 2,
        "value": "Ligament tear of Knee",
        "disease_id": 2001
    },
    {
        "sub_question_id": 1002,
        "question_id": 2,
        "value": "Fracture Femur(thigh bone)",
        "disease_id": 2002
    },
    {
        "sub_question_id": 1003,
        "question_id": 2,
        "value": "Fracture Humerus (arm)",
        "disease_id": 2003
    },
    {
        "sub_question_id": 1004,
        "question_id": 2,
        "value": "Fracture Radius/Ulna (forearm)",
        "disease_id": 2004
    },
    {
        "sub_question_id": 1005,
        "question_id": 2,
        "value": "Fracture Tibia/Fibula (leg)",
        "disease_id": 2005
    },
    {
        "sub_question_id": 1006,
        "question_id": 2,
        "value": "Fracture (unspecified)",
        "disease_id": 2006
    },
    {
        "sub_question_id": 1007,
        "question_id": 2,
        "value": "Total Knee Replacement (TKR)",
        "disease_id": 2007
    },
    {
        "sub_question_id": 1008,
        "question_id": 2,
        "value": "Total Hip Replacement(THR)",
        "disease_id": 2008
    },
    {
        "sub_question_id": 1009,
        "question_id": 2,
        "value": "Renal and ureteric calculus (Kidney Stone)",
        "disease_id": 2009
    },
    {
        "sub_question_id": 1010,
        "question_id": 2,
        "value": "Fibroid uterus (female only)",
        "disease_id": 2010
    },
    {
        "sub_question_id": 1011,
        "question_id": 2,
        "value": "Cholelithiasis (Gall bladder stone)",
        "disease_id": 2011
    },
    {
        "sub_question_id": 1012,
        "question_id": 2,
        "value": "Haemorrhoids (Piles)",
        "disease_id": 2012
    },
    {
        "sub_question_id": 1013,
        "question_id": 2,
        "value": "Inguinal Hernia (Hernia in groin)",
        "disease_id": 2013
    },
    {
        "sub_question_id": 1014,
        "question_id": 2,
        "value": "Appendicitis",
        "disease_id": 2014
    },
    {
        "sub_question_id": 1015,
        "question_id": 2,
        "value": "Cataract",
        "disease_id": 2015
    },
    {
        "sub_question_id": 1016,
        "question_id": 2,
        "value": "Deviated Nasal Septum",
        "disease_id": 2016
    },
    {
        "sub_question_id": 1017,
        "question_id": 2,
        "value": "Others",
        "disease_id": 2017
    },
    {
        "sub_question_id": 1018,
        "question_id": 3,
        "value": "Hypertension",
        "disease_id": 2018
    },
    {
        "sub_question_id": 1019,
        "question_id": 3,
        "value": "Dyslipidemia (High cholesterol)",
        "disease_id": 2019
    },
    {
        "sub_question_id": 1020,
        "question_id": 3,
        "value": "Anemia",
        "disease_id": 2020
    },
    {
        "sub_question_id": 1021,
        "question_id": 3,
        "value": "Hypothyroidism",
        "disease_id": 2021
    },
    {
        "sub_question_id": 1022,
        "question_id": 3,
        "value": "Hyperthyroidism",
        "disease_id": 2022
    },
    {
        "sub_question_id": 1023,
        "question_id": 3,
        "value": "Allergy",
        "disease_id": 2023
    },
    {
        "sub_question_id": 1024,
        "question_id": 3,
        "value": "Benign prostatic hypertrophy (BPH)/Benign Hyperplasia of Prostate",
        "disease_id": 2024
    },
    {
        "sub_question_id": 1025,
        "question_id": 3,
        "value": "Fibroadenoma breast (benign breast tumor)",
        "disease_id": 2025
    },
    {
        "sub_question_id": 1026,
        "question_id": 3,
        "value": "Acid peptic disease (Acidity and ulcers)",
        "disease_id": 2026
    },
    {
        "sub_question_id": 1027,
        "question_id": 3,
        "value": "Retinal Detachment",
        "disease_id": 2027
    },
    {
        "sub_question_id": 1028,
        "question_id": 3,
        "value": "Others",
        "disease_id": 2028
    },
    {
        "sub_question_id": 1029,
        "question_id": 4,
        "value": "Gout/hyperuricemia",
        "disease_id": 2029
    },
    {
        "sub_question_id": 1030,
        "question_id": 4,
        "value": "Polio (Residual poliomyelitis)",
        "disease_id": 2030
    },
    {
        "sub_question_id": 1031,
        "question_id": 4,
        "value": "Disc prolapse (PIVD / Slip Disc)",
        "disease_id": 2031
    },
    {
        "sub_question_id": 1032,
        "question_id": 4,
        "value": "Osteoarthritis",
        "disease_id": 2032
    },
    {
        "sub_question_id": 1033,
        "question_id": 4,
        "value": "Spondylitis",
        "disease_id": 2033
    },
    {
        "sub_question_id": 1034,
        "question_id": 4,
        "value": "Back Pain",
        "disease_id": 2034
    },
    {
        "sub_question_id": 1035,
        "question_id": 4,
        "value": "Blindness",
        "disease_id": 2035
    },
    {
        "sub_question_id": 1036,
        "question_id": 4,
        "value": "Hearing Loss",
        "disease_id": 2036
    },
    {
        "sub_question_id": 1037,
        "question_id": 4,
        "value": "Others",
        "disease_id": 2037
    },
    {
        "sub_question_id": 1038,
        "question_id": 5,
        "value": "Tuberculosis (TB)",
        "disease_id": 2038
    },
    {
        "sub_question_id": 1039,
        "question_id": 5,
        "value": "Asthma",
        "disease_id": 2039
    },
    {
        "sub_question_id": 1040,
        "question_id": 5,
        "value": "Allergic bronchitis",
        "disease_id": 2040
    },
    {
        "sub_question_id": 1041,
        "question_id": 5,
        "value": "Chronic Sinusitis",
        "disease_id": 2041
    },
    {
        "sub_question_id": 1042,
        "question_id": 5,
        "value": "Migraine",
        "disease_id": 2042
    },
    {
        "sub_question_id": 1043,
        "question_id": 5,
        "value": "Others",
        "disease_id": 2043
    },
    {
        "sub_question_id": 1044,
        "question_id": 0,
        "value": "",
        "disease_id": 0
    },
    {
        "sub_question_id": 1045,
        "question_id": 0,
        "value": "",
        "disease_id": 0
    },
    {
        "sub_question_id": 1046,
        "question_id": 0,
        "value": "",
        "disease_id": 0
    }
]

const medicalQuestionMaster = {
    2: `Have you undergone any surgery OR hospitalization for more than 10 days at a time in the
past OR are you awaiting any treatment or surgery that you have been advised`,
    3: `Have you been consulting a doctor regularly for any disease or complaint OR been under
any medication regularly for more than 2 weeks or noticed any growth or tumor in the
body?`,
    4: `Have you experienced pain for more than 7 days in any part of body OR restriction of any
movement OR difficulty in swallowing or breathing OR any difficulty in carrying out your
daily activities?`,
    5: `Did you ever have fits, HIV (Human Immune deficiency virus), persistent headache or persistent
cough OR blood in stool (frequency) or any bleeding from any other orifice / body opening for
more than 5 days?`,
    71: "Hypertension, Chest pain, Ischemic heart disease or any other cardiac disorder?",
    72: "Tuberculosis, Asthma, Bronchitis or any other lung/respiratory disorder?",
    73: "Ulcer (stomach/duodenal), Hepatitis, Cirrhosis or any other digestive or liver/ gallbladder disorder?",
    74: "Renal failure, Calculus or any other kidney/urinary tract or prostate disorder?",
    75: "Dizziness, Stroke, Epilepsy, Paralysis or other brain/ nervous system disorder?",
    76: "Diabetes, Thyroid disorder or any other endocrine disorder?",
    77: "Tumor-benign or malignant, any ulcer/growth/cyst?",
    78: "Arthritis, Spondylosis or any other disorder of the muscle/bone/joint?",
    79: "Diseases of the Nose/Ear/Throat/Teeth/ Eye ( please mention Diopters )?",
    80: "HIV/AIDS or sexually transmitted diseases or any immune system disorder?",
    81: "Anaemia, Leukaemia or any other blood/lymphatic system disorder?",
    82: "Psychiatric/Mental illnesses or Sleep disorder?",
    83: "DUB, Fibroid, Cyst/Fibroadenoma or any other Gynaecological/Breast disorder?",
    84: "Been addicted to alcohol, narcotics, habit forming drugs or been under detoxication therapy?",
    85: "Been under any regular medication (self/ prescribed)?",
    86: "Undertaken any lab/blood tests, imaging tests viz. scans/MRI in the last 5 years?",
    87: "Undertaken any surgery or been advised surgery in the last 10 years or have a surgery still pending?",
    88: "Suffered from any other disease/illness/accident/injury?",
    89: "Been informed that they are Pregnant? If yes, please mention the expected date of delivery?",
    90: "Had any complaint of Diabetes, Hypertension or any complication during current or earlier pregnancy?"
}

const occupations = [
    {
        occupCd: "400001",
        occupVal: "OC1"
    },
    {
        occupCd: "400002",
        occupVal: "OC2"
    },
    {
        occupCd: "400003",
        occupVal: "OC3"
    },
    {
        occupCd: "400004",
        occupVal: "OC4"
    },
    {
        occupCd: "400005",
        occupVal: "OC5"
    },
    {
        occupCd: "400006",
        occupVal: "OC6"
    }
]

const relationshipToPayorMaster =
{
    1: "Self",
    2: "Father",
    3: "Mother",
    6: "Son",
    7: "Daughter",
    5: "Spouse"
}


module.exports.intToRoman = intToRoman;
module.exports.medicalJson = medicalJson;
module.exports.professionMaster = professionMaster;
module.exports.relationshipMaster = relationshipMaster;
module.exports.maritalStatusMaster = maritalStatusMaster;
module.exports.stateMaster = stateMaster;
module.exports.medicalQuestionMaster = medicalQuestionMaster;
module.exports.occupations = occupations;
module.exports.relationshipToPayorMaster = relationshipToPayorMaster;