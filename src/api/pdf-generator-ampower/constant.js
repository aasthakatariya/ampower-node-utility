import { createSubQuestions} from './utils';

const commonTemplates = [
    {
        htmlFileName: 'proposal_top.html',
        footer: {
            height: '1.50cm',
            contents:
                `<div id="pageFooter" style="">
<div style="background-color: orangered; height: 1px; width:100%;"></div>
<div class="circle" style="margin: auto; background-color: white; position: relative;top: -9px;">
</div>
<div style="position: relative; text-align: center; top: -22px; font-weight:bold;">{{page}} <div style="display: none;"> of {{pages}} </div></div>
</div>`
        }
    },
    {
        htmlFileName: 'neft.html',
        // footer:``
    },
    {
        htmlFileName: 'proposal.html',
        // footer:``
    },
    {
        htmlFileName: 'portability_form.html',

        footer: {
            height: '4.50cm',
            contents:
                `<div id="pageFooter" style="padding: 25px;">

<div style="padding: 5px; background-color: orangered; color: white;">
    <div class="para" style="text-align: center; color: white;">
    We would be happy to assist you. Contact us at: Email: customerservice@apollomunichinsurance.com. Call Toll Free No.:
    1800 102 0333
    </div>
</div>
<div class="para">
    Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
    Udyog Vihar, Phase-III, Gurgaon-122016, Haryana • Corp. Off. 1st Floor, SCF19, Sector-14, Gurgaon-122001, Haryana • Reg.
    Off. Apollo Hospitals Complex, 8-2-293/82/J III/DH/900 Jubilee Hills, Hyderabad, Telangana - 500033 • For more details
    on risk factors, terms and
    conditions, please read sales brochure carefully before concluding a sale • IRDAI Reg. No. - 131 • CIN:
    U66030TG2006PLC051760
</div>

</div>`
        }
    }

]



const master = [
    {
        templateName: 'OPTIMA_RESTORE',
        templates: addTemplate(1, {
            htmlFileName: 'optima_restore.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760
        • Critical Advantage Rider UIN:IRDAI/NL-HLT/AMHI/P-H/V.I/59/14-15 • Optima Restore UIN: APOHLlP18125VO41718 • URN:
        AM/HLT/0067/A/122018 • Individual Personal Accident Rider UIN: APOPAIP19004V011920 • Protector Rider UIN: APOHLIP19006V011920
        • Hospital Daily Cash Rider UIN: APOHLIP19013V011920
    </div>
    
    </div>`
            }
        })
    },
    {
        templateName: 'HEALTH_WALLET',
        templates: addTemplate(1, {
            htmlFileName: 'health_wallet.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760
        • Critical Advantage Rider UIN:IRDAI/NL-HLT/AMHI/P-H/V.I/59/14-15 • Health Wallet UIN: IRDAI/HLT/AMHI/P-H/V.I/57/2016-17 • URN:
        AM/HLT/0067/A/122018
    </div>
    
    </div>`
            }
        })
    }, {
        templateName: 'EASY_HEALTH',
        templates: addTemplate(1, {
            htmlFileName: 'easy_health.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760
        • Critical Advantage Rider UIN:IRDAI/NL-HLT/AMHI/P-H/V.I/59/14-15 • Easy Health UIN: IRDAI/HLT/AMHI/P-H/V.III/1R/2016-17 • URN:
        AM/HLT/0067/A/122018 • Hospital Daily Cash Rider UIN: APOHLIP19013V011920 • Protector Rider UIN: APOHLIP19006V011920
        • Individual Personal Accident Rider UIN: APOPAIP19004V011920
    </div>
    
    </div>`
            }
        })
    },
    {
        templateName: 'MAXIMA',
        templates: addTemplate(1, {
            htmlFileName: 'maxima.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760
        • Critical Advantage Rider UIN:IRDAI/NL-HLT/AMHI/P-H/V.I/59/14-15 • Optima Cash UIN: APOHLlP18125VO41718 • URN:
        AM/HLT/0067/A/122018
    </div>
    
    </div>`
            }
        })
    },
    {
        templateName: 'OPTIMA_CASH',
        templates: addTemplate(1, {
            htmlFileName: 'optima_cash.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760
        • Optima Cash UIN: IRDA/NL-HLT/AMHI/P-H/V.I/227/13-14 • URN:
        AM/HLT/0067/A/122018
    </div>
    
    </div>`
            }
        })
    },
    {
        templateName: 'OPTIMA_PLUS',
        templates: addTemplate(1, {
            htmlFileName: 'optima_plus.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760
        • Critical Advantage Rider UIN:IRDAI/NL-HLT/AMHI/P-H/V.I/59/14-15 • Optima Plus UIN: IRDA/NL-HLT/AMHI/P-H/V.I/3/13-14 • URN:
        AM/HLT/0067/A/122018
    </div>
    
    </div>`
            }
        })
    },
    {
        templateName: 'OPTIMA_SUPER',
        templates: addTemplate(1, {
            htmlFileName: 'optima_super.html',
            footer: {
                height: '4.50cm',
                contents: ` <div id="pageFooter" style="padding: 20px;">
    
    <div style="padding: 5px; background-color: orangered; color: white;">
        <div class="para" style="text-align: center; color: white;">
            We would be happy to assist you. For any help contact us at: Email: customerservice@apollomunichinsurance.com Toll Free:
            1800 102 0333
        </div>
    </div>
    <div class="para">
        Apollo Munich Health Insurance Co. Ltd. • Central Processing Center, 2nd & 3rd Floor, iLABS Centre, Plot No. 404-405,
        Udyog Vihar, Phase-III, Gurgaon-122016, Haryana
        • Corp. Off. 1st Floor, SCF-19, Sector-14, Gurgaon-122001, Haryana • Reg. Off. Apollo Hospitals Complex, 8-2-293/82/J
        III/DH/900 Jubilee Hills, Hyderabad-500033, Telangana
        • For more details on risk factors, terms and conditions, please read sales brochure carefully before concluding a sale
        • IRDAI Reg. No.: - 131 • CIN: U66030TG2006PLC051760 • Optima Super UIN: IRDA/NL-HLT/AMHI/P-H(C)/V.1/9/13-14 • URN:
        AM/HLT/0067/A/122018
    </div>
    
    </div>`
            }
        })
    },
    {
        templateName: "PORTABILITY",
        templates: commonTemplates.filter(ele => ele.htmlFileName.toLowerCase().trim() =="portability_form.html")
        
    }
];


function addTemplate(index, template) {
    let templates = Object.create(commonTemplates)
    templates.splice(index, 0, template);
    return templates;
}

const proposalQuestions = [{
    desc: "Have you undergone any surgery OR hospitalization for more than 10 days at a time in the past OR are you awaiting any treatment or surgery that you have been advised",
    arr: {
        '1001': { text: 'Ligament tear of Knee' },
        '1002': { text: 'Fracture Femur(thigh bone)' },
        '1003': { text: "Fracture Humerus(arm)" },
        '1004': { text: "Fracture Radius / Ulna(forearm)" },
        '1005': { text: "Fracture Tibia / Fibula(leg)" },
        '1006': { text: "Fracture(unspecified)" },
        '1007': { text: "Total Knee Replacement(TKR)" },
        '1008': { text: "Total Hip Replacement(THR)" },
        '1009': { text: "Renal and ureteric calculus(Kidney Stone)" },
        '1010': { text: "Fibroid uterus(female only)" },
        '1011': { text: "Cholelithiasis(Gall bladder stone)" },
        '1012': { text: "Haemorrhoids(Piles)" },
        '1013': { text: "Inguinal Hernia(Hernia in groin)" },
        '1014': { text: "Appendicitis" },
        '1015': { text: "Cataract" },
        '1016': { text: "Deviated Nasal Septum" },
    }
}, {
    desc: "Have you been consulting a doctor regularly for any disease or complaint OR been under any medication regularly for more than 2 weeks or noticed any growth or tumor in the body?",
    arr: {
        '1018': { text: "Hypertension" },
        '1019': { text: "Dyslipidemia (High cholesterol)" },
        '1020': { text: "Anemia" },
        '1021': { text: "Hypothyroidism" },
        '1023': { text: "Allergy" },
        '1022': { text: "Hyperthyroidism" },
        '1024': { text: "Benign prostatic hypertrophy (BPH)/Benign Hyperplasia of Prostate Fibroadenoma breast (benign breast tumor)" },
        '1026': { text: "Acid peptic disease (Acidity and ulcers)" },
        '1027': { text: "Retinal Detachment" },
    },
},
{
    desc: "Have you experienced pain for more than 7 days in any part of body OR restriction of any movement OR difficulty in swallowing or breathing OR any difficulty in carrying out your daily activities?",
    arr: {
        '1029': { text: "Gout / hyperuricemia" },
        '1030': { text: "Polio(Residual poliomyelitis)" },
        '1031': { text: "Disc prolapse(PIVD / Slip Disc)" },
        '1032': { text: "Osteoarthritis" },
        '1033': { text: "Spondylitis" },
        '1034': { text: "Back Pain" },
        '1035': { text: "Blindness" },
        '1036': { text: "Hearing Loss" },
    },
},
{
    desc: "Did you ever have fits, HIV (Human Immune deficiency virus), persistent headache or persistent cough OR blood in stool (frequency) or any bleeding from any other orifice / body opening for more than 5 days?",
    arr: {
        '1038': { text: "Tuberculosis (TB)" },
        '1039': { text: "Asthma" },
        '1040': { text: "Allergic bronchitis" },
        '1041': { text: "Chronic Sinusitis" },
        '1042': { text: "Migraine" },
    }
}]

let relationShipMaster = [
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 18,
        "relationship_value": "Father-in-law"
    },
    {
        "relationship_key": 19,
        "relationship_value": "Mother-in-law"
    },
    {
        "relationship_key": 21,
        "relationship_value": "Grand Father"
    },
    {
        "relationship_key": 22,
        "relationship_value": "Grand Mother"
    },
    {
        "relationship_key": 23,
        "relationship_value": "Grand Son"
    },
    {
        "relationship_key": 24,
        "relationship_value": "Grand Daughter"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 18,
        "relationship_value": "Father-in-law"
    },
    {
        "relationship_key": 19,
        "relationship_value": "Mother-in-law"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 18,
        "relationship_value": "Father-in-law"
    },
    {
        "relationship_key": 19,
        "relationship_value": "Mother-in-law"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 20,
        "relationship_value": "Son"
    },
    {
        "relationship_key": 17,
        "relationship_value": "Daughter"
    },
    {
        "relationship_key": 2,
        "relationship_value": "Child"
    },
    {
        "relationship_key": 15,
        "relationship_value": "Husband"
    },
    {
        "relationship_key": 14,
        "relationship_value": "Wife"
    },
    {
        "relationship_key": 99,
        "relationship_value": "Policy Holder"
    },
    {
        "relationship_key": 4,
        "relationship_value": "Father"
    },
    {
        "relationship_key": 5,
        "relationship_value": "Mother"
    },
    {
        "relationship_key": 18,
        "relationship_value": "Father-in-law"
    },
    {
        "relationship_key": 19,
        "relationship_value": "Mother-in-law"
    }
]

module.exports.master = master;
module.exports.proposalQuestions = proposalQuestions;
module.exports.relationShipMaster = relationShipMaster;