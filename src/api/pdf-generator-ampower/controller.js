import {
  successResponse,
  errorResponse,
  downloadFile,
  downloadFileAndCleanup
} from '../../services/response/';

import { genericPdf, testPdf, generatetranscriptAmPower } from './service';
import { mock_data } from './mock_data';
let fs = require('fs');
import { promisify } from 'util';
fs.unlink = promisify(fs.unlink);


const GenericPdfCtrlForAmPower = async ({ bodymen: { body } }, res) => {
  let cleanupFiles = [];
  try {
    // mock_data.templateName = body.templateName;
    // let path = await generatetranscriptAmPower(mock_data);

    let params = body.origin == 'SERVICE' ? JSON.parse(body.data) : body.data;
    params.templateName = body.templateName;
    // console.log("REQUEST -------------------------_-----");
    // console.log(JSON.stringify(params, null, 4));
    // console.log("REQUEST -------------------------_-----");

    let path = await generatetranscriptAmPower(params);
    if (path && path.cleanupFiles && path.cleanupFiles.length > 0) {
      cleanupFiles = path.cleanupFiles;
    }
    downloadFileAndCleanup(res, path.filename, path.cleanupFiles, true);

  } catch (err) {
    console.log(err);
    errorResponse(res, err);
  }
};

module.exports = { GenericPdfCtrlForAmPower };
