import { Router } from 'express'
import { middleware as query } from 'querymen';
import { middleware as body } from 'bodymen';
import { testGenericPdfCtrl, GenericPdfCtrlForAmPower } from './controller';

const router = new Router()


router.post('/generatePdf', body({
    templateName: {
        type: String,
        enum: ['OPTIMA_RESTORE', "EASY_HEALTH", "HEALTH_WALLET", "MAXIMA", "OPTIMA_CASH", "OPTIMA_PLUS", "OPTIMA_SUPER", "PORTABILITY"],
        required: true
    },
    data: {
        type: Object,
    },
    origin: {
        type: String,
        enum: ["SERVICE", "POSTMAN"],
        required: true
    }
}), GenericPdfCtrlForAmPower)

export default router
