// export let mock_data = {
//     "_id": 'ObjectId("5beed6025e9dc071929fde36")',
//     "updatedAt": 'ISODate("2018-11-16T14:43:14.558Z")',
//     "createdAt": 'ISODate("2018-11-16T14:36:50.506Z")',
//     "quotation": {
//         "city": {
//             "code": "778",
//             "city": "Delhi",
//             "stateCode": "010"
//         },
//         "memberCoveredInIndividual": [
//             {
//                 "baseProductSI": {
//                     "value": "5",
//                     "code": "BSI-02",
//                     "conversion": "500000"
//                 },
//                 "age": "38",
//                 "dateOfBirth": "08/08/1980",
//                 "_id": 'ObjectId("5beed6025e9dc071929fde39")'
//             }
//         ],
//         "memberCoveredInFloater": {
//             "member": []
//         },
//         "product": 'ObjectId("5bd162b6b8ced947a4240b80")',
//         "productCode": "34011",
//         "externalReferenceCode": "11119",
//         // "productName": "Optima Restore",
//         "productName": "iCan",
//         "productTenure": 1,
//         "coverageType": "individual",
//         "memberCoveredIndividual": 1,
//         "premiumResponse": {
//             "res": {
//                 "TotalPremium": "9540.3",
//                 "Client": {
//                     "Product": {
//                         "LoadingAmount": "0.00",
//                         "SourceCode": "Portal",
//                         "SumAssured": "500000",
//                         "TaxAmount": "1455.30",
//                         "GrossPremiumAmount": "9540.30",
//                         "DiscountAmount": "0.00",
//                         "BasePremiumAmount": "8085.00",
//                         "ProductGroup": "1",
//                         "ProductLine": "9",
//                         "ProductType": "1",
//                         "SACCode": "1",
//                         "ProductVersion": "1",
//                         "ProductCode": "34011",
//                         "ClientCode": "Client1"
//                     },
//                     "Address": {
//                         "Town": "778"
//                     },
//                     "ClientCode": "Client1",
//                     "Age": "38",
//                     "OccuptionCode": "0"
//                 },
//                 "xmlns:p718": "http://www.apollomunichinsurance.com/businessservices/premiumcalculator"
//             },
//             "err": null
//         },
//         "tax": "1455.30",
//         "discount": "0.00",
//         "netPremium": "8085.00",
//         "totalPremium": "9540.3",
//         "optimaRestoreAmount": "8085.00",
//         "CARiderAmount": "0.00",
//         "_id": 'ObjectId("5beed6025e9dc071929fde38")'
//     },
//     "partnerId": 'ObjectId("5bbca0e35ef8d052877d7f7e")',
//     "customerName": "Amit",
//     "customerEmail": "amit.panghal@geminisolutions.in",
//     "customerPhoneNumber": "8059533405",
//     "isEmail": false,
//     "partnerName": "HDFC",
//     "partnerProductId": 'ObjectId("5bdff8724b764d3ad46036d0")',
//     // "fProductName": "Optima Restore individual 1 year",
//     "productName": "iCan",
//     "coverType": "individual",
//     "dateOfBirth": "08/08/1980",
//     "cityInRoot": "Ahiwara",
//     "totalPremium": "9019.92",
//     "leadNumber": "AMHI161118000176",
//     "isQuote": false,
//     "onlinePayment_OrderHistory": [
//         {
//             "order_No": "LMS469571264",
//             "order_Id": "ord_c1c6b4b387ab4c5ebe8f020a2f52c171",
//             "createdAt": 'ISODate("2018-11-16T14:42:42.116Z")',
//             "status": "CREATED",
//             "status_id": "1",
//             "_id": 'ObjectId("5beed7625e9dc071929ff02b")'
//         }
//     ],
//     "transactions": [
//         {
//             "date": 'ISODate("2018-11-16T14:43:11.000Z")',
//             "amount": "2",
//             "remark": "Dear Customer, Your transaction is Successful",
//             "paymentMode": "CARD",
//             "transactionGatewayId": "16",
//             "payload": {
//                 "udf9": "",
//                 "udf8": "",
//                 "udf7": "",
//                 "udf6": "",
//                 "udf5": "",
//                 "udf4": "",
//                 "udf3": "",
//                 "udf2": "",
//                 "udf10": "",
//                 "udf1": "",
//                 "txn_uuid": "mrypgkliifqyqxwa",
//                 "txn_id": "LMS469571264",
//                 "status_id": 21,
//                 "status": "CHARGED",
//                 "return_url": "http://13.232.58.217/payment/PaymentResponse",
//                 "refunded": false,
//                 "product_id": "",
//                 "payment_method_type": "CARD",
//                 "payment_method": "VISA",
//                 "payment_links": {
//                     "web": "https://sandbox.juspay.in/merchant/pay/ord_c1c6b4b387ab4c5ebe8f020a2f52c171",
//                     "mobile": "https://sandbox.juspay.in/merchant/pay/ord_c1c6b4b387ab4c5ebe8f020a2f52c171?mobile=true",
//                     "iframe": "https://sandbox.juspay.in/merchant/ipay/ord_c1c6b4b387ab4c5ebe8f020a2f52c171"
//                 },
//                 "payment_gateway_response": {
//                     "txn_id": "LMS469571264",
//                     "rrn": "1542379389158",
//                     "resp_message": "Y",
//                     "resp_code": "Success",
//                     "epg_txn_id": "307004394797",
//                     "created": "2018-11-16T14:43:11Z",
//                     "auth_id_code": "NA"
//                 },
//                 "order_id": "LMS469571264",
//                 "merchant_id": "Apollo_Munich",
//                 "id": "ord_c1c6b4b387ab4c5ebe8f020a2f52c171",
//                 "gateway_id": 16,
//                 "date_created": "2018-11-16T14:42:42Z",
//                 "customer_phone": "8059533405",
//                 "customer_id": "21549920P",
//                 "customer_email": "amit.panghal@geminisolutions.in",
//                 "currency": "INR",
//                 "card": {
//                     "using_saved_card": false,
//                     "saved_to_locker": false,
//                     "name_on_card": "adsadasd",
//                     "last_four_digits": "",
//                     "expiry_year": "2020",
//                     "expiry_month": "10",
//                     "card_type": "DEBIT",
//                     "card_reference": "",
//                     "card_issuer": "",
//                     "card_isin": "424242",
//                     "card_fingerprint": "78g5l1fp62obpivbqsved6dsgb",
//                     "card_brand": "VISA"
//                 },
//                 "bank_error_message": "",
//                 "bank_error_code": "",
//                 "auth_type": "THREE_DS",
//                 "amount_refunded": 0,
//                 "amount": 2
//             },
//             "isSuccessful": false,
//             "isShortPremium": false,
//             "transactionAmount": "2",
//             "transactionNumber": "LMS469571264",
//             "transactionDate": 'ISODate("2018-11-16T14:43:11.000Z")',
//             "_id": 'ObjectId("5beed7825e9dc071929ff036")'
//         }
//     ],
//     "isMedicalVisited": true,
//     "isLifestyleVisited": true,
//     "history": [
//         {
//             "reason": "Quotation Created",
//             "timestamp": "Fri Nov 16 2018 14:36:50 GMT+0000 (UTC)",
//             "status": "Lead Created",
//             "_id": 'ObjectId("5beed6025e9dc071929fde3a")'
//         },
//         {
//             "doneBy": {
//                 "AVCode": "102018@hdfc"
//             },
//             "timestamp": "Fri Nov 16 2018 14:41:49 GMT+0000 (UTC)",
//             "status": "Call Back",
//             "_id": 'ObjectId("5beed72d5e9dc071929fe93e")'
//         },
//         {
//             "doneBy": {
//                 "AVCode": "102018@hdfc"
//             },
//             "timestamp": "Fri Nov 16 2018 14:42:26 GMT+0000 (UTC)",
//             "status": "UW Accepted",
//             "_id": 'ObjectId("5beed7525e9dc071929fedd1")'
//         }
//     ],
//     "currentPage": {
//         "header": "review-detail",
//         "url": "/app/lead/review-detail"
//     },
//     "assignedTo": [
//         {
//             "AVCode": "Tele@hdfc",
//             "timestamp": 'ISODate("2018-11-16T14:36:50.491Z")',
//             "by": "Tele@hdfc",
//             "remark": "Initial lead creator",
//             "_id": 'ObjectId("5beed6025e9dc071929fde37")'
//         }
//     ],
//     "creator": {
//         "name": "Gaurang Omar",
//         "id": 'ObjectId("5bcee5a417878229f5fef0b2")',
//         "AVCode": "Tele@hdfc"
//     },
//     "queueStatus": {
//         "status": "Policy Issued",
//         "remark": "Your policy will be issued within three hours."
//     },
//     "status": {
//         "leadStatus": {
//             "code": "leadStatus-01",
//             "status": "AV Sales closed"
//         },
//         "subStatus": {
//             "status": "UW Accepted",
//             "code": "subStatus-01"
//         }
//     },
//     "applicationNumberHistory": [
//         {
//             "applicationNumber": "21549920P",
//             "date": "Fri Nov 16 2018 14:39:33 GMT+0000 (UTC)",
//             "totalPremium": "9019.92",
//             "_id": 'ObjectId("5beed6a55e9dc071929fe0cd")'
//         }
//     ],
//     "channelAVConcent": true,
//     "member": [{
//         "title": {
//             "id": "5c665fb237feb26f11199d77",
//             "title": "Mr.",
//             "code": "MR",
//             "isActive": true
//         },
//         "firstName": "A SAHA",
//         "lastName": "SAHA",
//         "dateOfBirth": "04/05/1990",
//         "gender": {
//             "code": "1",
//             "gender": "Male"
//         },
//         "relationshipWithProposer": {
//             "code": "1",
//             "relationship": "Self"
//         },
//         "height": {
//             "feet": "5",
//             "inch": "9"
//         },
//         "weight": "65",
//         "smokerFlag": "false",
//         "occupation": {
//             "code": "303601",
//             "occupationName": "ACCOUNTANT",
//             "occupationClass": "PA_CL_1",
//             "occupationClassCode": "613505",
//             "isActive": true,
//             "id": "5c665eb237feb26f11195164"
//         },
//         "baseSumInsured": {
//             "conversion": "500000",
//             "code": "BSI-02",
//             "value": "5"
//         },
//         "maritalStatus": {
//             "maritalStatus": "Single",
//             "code": "2",
//             "isActive": true,
//             "id": "5c665e8237feb26f11195093"
//         },
//         "product": [{
//             "clientCode": "Client1",
//             "productCode": "45011",
//             "productVersion": "1",
//             "SACCode": 1,
//             "SACCounter": "0",
//             "productType": "1",
//             "productName": "iCan Individual 1 Year",
//             "productLine": "2",
//             "productGroup": "1",
//             "CBAmount": "0",
//             "sumAssured": "500000",
//             "sourceCode": "Portal",
//             "paymentFrequeny": "1",
//             "paymentMode": "14"
//         }],
//         "medical": [{
//             "code": "MQ-18",
//             "text": "Have you ever been diagnosed with any form of cancer or tumour (includes Lymphoma, Leukaemia, and Sarcoma)?",
//             "answer": "No",
//             "subQuestions": []
//         }, {
//             "code": "MQ-19",
//             "text": "Have you ever been suspected to have or investigated for cancer or have been under follow up for cancer or a condition that doctors suspected may become cancerous?",
//             "answer": "No",
//             "subQuestions": []
//         }, {
//             "code": "MQ-20",
//             "text": "Have you ever been diagnosed with Human Immunodeficiency virus (HIV), Human papillomavirus infection virus (HPV), Epstein–Barr virus (EBV), Hepatitis B or Hepatitis C",
//             "answer": "No",
//             "subQuestions": []
//         }, {
//             "code": "MQ-21",
//             "text": "Have you ever experienced weight loss of more than 5 Kilos over 3 months.",
//             "answer": "No",
//             "subQuestions": []
//         }, {
//             "code": "MQ-22",
//             "text": "Did you ever had blood in stool or any bleeding from any other opening for more than 5 days OR fits, Persistent headache or cough in the last 2 years.",
//             "answer": "No",
//             "subQuestions": []
//         }, {
//             "code": "MQ-23",
//             "text": "Has any of your Parents or Siblings or more than one of your parent’s siblings (including your parents) been diagnosed with cancer?",
//             "answer": "No",
//             "subQuestions": []
//         }],
//         "lifestyle": [{
//             "code": "LQ-07",
//             "text": "Does your occupation expose you to radiation, corrosive substances, harmful chemicals, mining, asbestos or explosives?",
//             "subQuestions": []
//         }, {
//             "code": "LQ-08",
//             "text": "Have you used tobacco in any form in the last one year? E.g Smoked Beedi , cigarette, Cigar, Cheroot, sheesha ?",
//             "subQuestions": [{
//                 "text": "Number of Units (1 Unit = 1 Beedi/Cigarette/Cigar/Cheeroot/Sheesha per day)"
//             }, {
//                 "text": "Consuming since age of"
//             }]
//         }, {
//             "code": "LQ-09",
//             "text": "Have you used chewing tobacco in the last one year? (Pan/Gutkha); Snuff etc",
//             "subQuestions": [{
//                 "text": "Number of Units (1 Unit = No. of Masala/Gutkha Pouches per day)"
//             }, {
//                 "text": "Consuming since age of"
//             }]
//         }, {
//             "code": "LQ-10",
//             "text": "Do you consume alcohol, or any narcotic/ habit forming/recreational drug? ",
//             "subQuestions": [{
//                 "text": "Number of Units (1 Unit = 30 ml of hard liquor; 150ml of wine; 330ml of beer, per week)"
//             }, {
//                 "text": "Consuming since age of"
//             }]
//         }],
//         "isAgeBandChange": "false"
//     }],
//     "__v": 0,
//     "account": {
//         "cardRelationshipNumber": "gdfg",
//         "last4DigitNumber": 3345,
//         "bankVerifierId": "tele",
//         "alternateAccountNumber": "fdsdsfsfs",
//         "caseId": "534534",
//         "_id": 'ObjectId("5beed6a45e9dc071929fe09d")'
//     },
//     "address": {
//         "ccAddress": "No",
//         "address1": "CP ",
//         "address2": "Nathu road",
//         "address3": "phase 3",
//         "country": {
//             "code": "IN",
//             "country": "India"
//         },
//         "state": {
//             "state": "DELHI",
//             "code": "010",
//             "stateCode": "010"
//         },
//         "city": {
//             "city": "New Delhi",
//             "code": "832",
//             "stateCode": "010"
//         },
//         "district": {
//             "districtName": "Central Delhi",
//             "code": "122",
//             "stateCode": "010"
//         },
//         "pinCode": "110002",
//         "timeToContact": {}
//     },
//     "applicationNumber": "21549920P",
//     "general": {
//         "AMHILocation": {
//             "city": "Garacharma",
//             "code": "002"
//         },
//         "partnerLocation": {
//             "city": "Garacharma",
//             "code": "002"
//         },
//         "subChannel": {
//             "code": "COP",
//             "name": "COP"
//         },
//         "IMDCode": "HDFC-IMD-03",
//         "campaign1": "No",
//         "campaign2": "No",
//         "AVCode": "Tele@hdfc",
//         "_id": 'ObjectId("5beed6a45e9dc071929fe09c")',
//         "SPCode": "SP6",
//         "SPName": "SP1"
//     },
//     "isProposerCovered": true,
//     "isReviewPaymentVisited": true,
//     "leadInfoDetail": {
//         "leadSubStatus": {
//             "code": "subStatus-01",
//             "status": "UW Accepted"
//         },
//         "leadNumber": "AMHI161118000176",
//         "typeOfCase": "STP",
//         "AMHIAVRemark": "UW Accepted - xZX",
//         "_id": 'ObjectId("5beed6a45e9dc071929fe0b7")'
//     },
//     "nominee": {
//         "title": {
//             "title": "Dr",
//             "code": "Dr"
//         },
//         "relationshipWithProposer": {
//             "code": "4",
//             "relationship": "Father"
//         },
//         "fullName": "gdfgdfgdf",
//         "address": "fsdfsd, Bilaspur, Ahiwara, Chhattisgarh, India, 110089",
//         "addressSameAsProposer": true,
//         "_id": 'ObjectId("5beed6a45e9dc071929fe0b5")'
//     },
//     "payment": {
//         "modeOfPayment": {
//             "paymentMode": "PG / Debit Card",
//             "code": "11"
//         },
//         "emiOption": "No",
//         "autoRenewal": "Yes",
//         "_id": 'ObjectId("5beed6a45e9dc071929fe0b6")'
//     },
//     "premiumDetail": {
//         "basePremium": {
//             "optimaRestore": "7644.00",
//             "CARider": "0.00"
//         },
//         "netPremium": 7644,
//         "GST": 1375.92,
//         "grossPremium": 9019.92,
//         "premiumDiscount": "0.00",
//         "_id": 'ObjectId("5beed6a45e9dc071929fe0b8")'
//     },
//     "product": {
//         "typeOfBusiness": {
//             "type": "New Business",
//             "code": "BT-01"
//         },
//         "product": 'ObjectId("5bd162b6b8ced947a4240b80")',
//         "productName": "Optima Restore",
//         "coverType": "individual",
//         "totalMember": "1",
//         "totalMemberValue": "1",
//         "policyTenure": "1",
//         "productCode": "34011",
//         "variant": 'Essential Advanced',
//         "allProductDetails": {
//             "id": "5bd162b6b8ced947a4240b80",
//             "subProduct": [
//                 {
//                     "productName": "Critical Advantage",
//                     "coverageType": "Rider",
//                     "productTenure": 1,
//                     "productCode": "11197",
//                     "externalReferenceCode": "11197",
//                     "productVersion": "1",
//                     "SACCounter": "0",
//                     "productType": "2",
//                     "productLine": "9",
//                     "productGroup": "1",
//                     "paymentFrequeny": "1",
//                     "paymentMode": "14",
//                     "_id": "5bed5066b5c1651f77c0ae0d"
//                 }
//             ],
//             "isActive": true,
//             "createdAt": "2018-11-15T10:54:20.149Z",
//             "updatedBy": "admin@admin.in",
//             "updatedAt": "2018-11-15T10:54:30.101Z",
//             "productVersion": "1",
//             "productType": "1",
//             "productLine": "9",
//             "productGroup": "1",
//             "paymentMode": "14",
//             "paymentFrequeny": "1",
//             "SACCounter": "0",
//             "externalReferenceCode": "11119",
//             "productCode": "34011",
//             "productTenure": 1,
//             "coverageType": "individual",
//             "productName": "Optima Restore",
//             "type": "0",
//             "parentProductCode": "OR-BP",
//             "__v": 1
//         },
//         "_id": 'ObjectId("5beed6a45e9dc071929fe09e")'
//     },
//     "proposer": {
//         "title": {
//             "id": "5c235d540868c37f219c2ad4",
//             "title": "Mr.",
//             "code": "MR",
//             "isActive": true
//         },
//         "firstName": "Airtel",
//         "middleName": "kumar",
//         "lastName": "Telesales",
//         "mobileNumber": "9015336265",
//         "email": "azad.verma@geminisolutions.in",
//         "dateOfBirth": "26/11/1993",
//         "gender": {
//             "id": "5c2357d44e76dc746ce89792",
//             "code": "1",
//             "gender": "Male",
//             "isActive": true
//         },
//         "IdType": {
//             "id": "5c2357f34e76dc746ce89795",
//             "code": "IDNO1",
//             "description": "Passport",
//             "isActive": true
//         },
//         "IdNumber": "456789",
//         "annualIncome": {
//             "_id": "5c58302a39cded55f100247c",
//             "annualIncome": "Less than 2.5 lac",
//             "code": "249999",
//             "isActive": true
//         },
//         "maritalStatus": {
//             "maritalStatus": "Single",
//             "code": "2",
//             "isActive": true,
//             "id": "5c2358514e76dc746ce897a5"
//         }
//     },
//     "proposalSubmissionDate": 'ISODate("2018-11-16T14:39:33.769Z")',
//     "paymentLinkDate": 'ISODate("2018-11-16T14:42:29.911Z")',
//     "onlinePayment_Order": {
//         "status_id": "1",
//         "status": "CREATED",
//         "createdAt": 'ISODate("2018-11-16T14:42:42.116Z")',
//         "order_Id": "ord_c1c6b4b387ab4c5ebe8f020a2f52c171",
//         "order_No": "LMS469571264"
//     },
//     "transactionAmount": "2",
//     "transactionDate": 'ISODate("2018-11-16T14:43:11.000Z")',
//     "transactionNumber": "LMS469571264",
//     "transactionStatus": "CHARGED",
//     "transactionStatusCode": "21",
//     "proposal": [{
//         desc: "Have you undergone any surgery OR hospitalization for more than 10 days at a time in the past OR are you awaiting any treatment or surgery that you have been advised",
//         arr: [
//             'Ligament tear of Knee',
//             'Fracture Femur(thigh bone)',
//             "Ligament tear of Knee",
//             "Fracture Femur(thigh bone)",
//             "Fracture Humerus(arm)",
//             "Fracture Radius / Ulna(forearm)",
//             "Fracture Tibia / Fibula(leg)",
//             "Fracture(unspecified)",
//             "Total Knee Replacement(TKR)",
//             "Total Hip Replacement(THR)",
//             "Renal and ureteric calculus(Kidney Stone)",
//             "Fibroid uterus(female only)",
//             "Cholelithiasis(Gall bladder stone)",
//             "Haemorrhoids(Piles)",
//             "Inguinal Hernia(Hernia in groin)",
//             "Appendicitis",
//             "Cataract",
//             "Deviated Nasal Septum",
//         ]
//     }, {
//         desc: "Have you been consulting a doctor regularly for any disease or complaint OR been under any medication regularly for more than 2 weeks or noticed any growth or tumor in the body?",
//         arr: [
//             "Hypertension",
//             "Dyslipidemia (High cholesterol)",
//             "Anemia",
//             "Hypothyroidism",
//             "Allergy",
//             "Hyperthyroidism",
//             "Benign prostatic hypertrophy (BPH)/Benign Hyperplasia of Prostate Fibroadenoma breast (benign breast tumor)",
//             "Acid peptic disease (Acidity and ulcers)",
//             "Retinal Detachment",
//         ],
//     },
//     {
//         desc: "Have you experienced pain for more than 7 days in any part of body OR restriction of any movement OR difficulty in swallowing or breathing OR any difficulty in carrying out your daily activities?",
//         arr: [
//             "Gout / hyperuricemia",
//             "Polio(Residual poliomyelitis)",
//             "Disc prolapse(PIVD / Slip Disc)",
//             "Osteoarthritis",
//             "Spondylitis",
//             "Back Pain",
//             "Blindness",
//             "Hearing Loss",
//         ],
//     },
//     {
//         desc: "Did you ever have fits, HIV (Human Immune deficiency virus), persistent headache or persistent cough OR blood in stool (frequency) or any bleeding from any other orifice / body opening for more than 5 days?",
//         arr: [
//             "Tuberculosis (TB)",
//             "Asthma",
//             "Allergic bronchitis",
//             "Chronic Sinusitis",
//             "Migraine",
//         ]
//     }]
// }
// //mock data for airtel OR

// // {
// // 	"partnerId": "5c6a8547f9db39453b992aa7",
// // 	"partnerProductId": "5c6a85fbf9db39453b992ab4",
// // 	"currentPage": {
// // 		"url": "/app/lead/proposer",
// // 		"header": "proposer",
// // 		"subHeader": 6
// // 	},
// // 	"general": {
// // 		"AMHILocation": {
// // 			"updatedAt": "2019-02-18T10:15:10.873Z",
// // 			"createdAt": "2019-02-18T10:15:10.873Z",
// // 			"partnerId": "5c6a8547f9db39453b992aa7",
// // 			"branchName": "Gurgaon",
// // 			"branchCode": "900001",
// // 			"isActive": true,
// // 			"channels": ["5c6a8575f9db39453b992aa8"],
// // 			"id": "5c6a85aef9db39453b992aa9"
// // 		},
// // 		"partnerLocation": {
// // 			"updatedAt": "2019-02-18T10:15:10.873Z",
// // 			"createdAt": "2019-02-18T10:15:10.873Z",
// // 			"partnerId": "5c6a8547f9db39453b992aa7",
// // 			"branchName": "Gurgaon",
// // 			"branchCode": "900001",
// // 			"isActive": true,
// // 			"channels": ["5c6a8575f9db39453b992aa8"],
// // 			"id": "5c6a85aef9db39453b992aa9"
// // 		},
// // 		"BDRCode": "TSE6",
// // 		"teamCode": "TL6",
// // 		"SMCode": "SM6",
// // 		"subChannel": {
// // 			"code": "VRM",
// // 			"name": "VRM"
// // 		},
// // 		"IMDCode": "IMD6",
// // 		"campaign1": "No",
// // 		"campaign2": "No",
// // 		"AVCode": "Airtel@airtel",
// // 		"SPCode": "SP6"
// // 	},
// // 	"account": {
// // 		"bankVerifierId": "Airtel"
// // 	},
// // 	"product": {
// // 		"product": "5c6a8490f9db39453b992aa3",
// // 		"typeOfBusiness": {
// // 			"id": "5bbb5a7b9c4c8341ebe415bf",
// // 			"type": "New Business",
// // 			"code": "BT-01",
// // 			"isActive": true,
// // 			"updatedBy": {}
// // 		},
// // 		"productName": "Optima Restore",
// // 		"coverType": "individual",
// // 		"totalMember": 1,
// // 		"totalMemberValue": 1,
// // 		"policyTenure": 1
// // 	},
// // 	"proposer": {
// // 		"title": {
// // 			"id": "5c665fb237feb26f11199d77",
// // 			"title": "Mr.",
// // 			"code": "MR",
// // 			"isActive": true
// // 		},
// // 		"firstName": "Deepak",
// // 		"lastName": "Kumar",
// // 		"mobileNumber": "8920643105",
// // 		"email": "azad.verma@geminisolutions.in",
// // 		"dateOfBirth": "24/01/1991",
// // 		"gender": {
// // 			"id": "5c665e1637feb26f1119506a",
// // 			"code": "1",
// // 			"gender": "Male",
// // 			"isActive": true
// // 		},
// // 		"IdType": {
// // 			"id": "5c665d6d37feb26f11194d4e",
// // 			"code": "IDNO1",
// // 			"description": "Passport",
// // 			"isActive": true
// // 		},
// // 		"IdNumber": "dfgh",
// // 		"annualIncome": {
// // 			"_id": "5c665d7d37feb26f11194d53",
// // 			"annualIncome": "Less than 2.5 lac",
// // 			"code": "249999",
// // 			"isActive": true
// // 		},
// // 		"maritalStatus": {
// // 			"maritalStatus": "Single",
// // 			"code": "2",
// // 			"isActive": true,
// // 			"id": "5c665e8237feb26f11195093"
// // 		}
// // 	},
// // 	"address": {
// // 		"ccAddress": "No",
// // 		"address1": "dfhgjhkj",
// // 		"country": {
// // 			"code": "IN",
// // 			"country": "India"
// // 		},
// // 		"state": {
// // 			"state": "DELHI",
// // 			"code": "010",
// // 			"stateCode": "010"
// // 		},
// // 		"city": {
// // 			"city": "New Delhi",
// // 			"code": "832",
// // 			"stateCode": "010"
// // 		},
// // 		"district": {
// // 			"districtName": "Central Delhi",
// // 			"code": "122",
// // 			"stateCode": "010"
// // 		},
// // 		"pinCode": "110002",
// // 		"timeToContact": {}
// // 	},
// // 	"nominee": {
// // 		"title": {
// // 			"id": "5c665fb237feb26f11199d77",
// // 			"title": "Mr.",
// // 			"code": "MR",
// // 			"isActive": true
// // 		},
// // 		"fullName": "dxfghjk",
// // 		"relationshipWithProposer": {
// // 			"id": "5c665f4437feb26f11199d23",
// // 			"code": "6",
// // 			"relationship": "Brother",
// // 			"isActive": true
// // 		},
// // 		"address": "dfhgjhkj, Central Delhi, New Delhi, DELHI, India, 110002",
// // 		"addressSameAsProposer": true
// // 	},
// // 	"member": [{
// // 		"title": {
// // 			"id": "5c665fb237feb26f11199d77",
// // 			"title": "Mr.",
// // 			"code": "MR",
// // 			"isActive": true
// // 		},
// // 		"firstName": "Deepak",
// // 		"lastName": "Kumar",
// // 		"dateOfBirth": "24/01/1991",
// // 		"age": 28,
// // 		"gender": {
// // 			"id": "5c665e1637feb26f1119506a",
// // 			"code": "1",
// // 			"gender": "Male",
// // 			"isActive": true
// // 		},
// // 		"relationshipWithProposer": {
// // 			"code": "1",
// // 			"relationship": "Self"
// // 		},
// // 		"height": {
// // 			"feet": "5",
// // 			"inch": "9"
// // 		},
// // 		"weight": "65",
// // 		"baseSumInsured": {
// // 			"value": "5",
// // 			"code": "BSI-02",
// // 			"conversion": "500000"
// // 		},
// // 		"maritalStatus": {
// // 			"maritalStatus": "Single",
// // 			"code": "2",
// // 			"isActive": true,
// // 			"id": "5c665e8237feb26f11195093"
// // 		},
// // 		"annualIncome": {
// // 			"_id": "5c665d7d37feb26f11194d53",
// // 			"annualIncome": "Less than 2.5 lac",
// // 			"code": "249999",
// // 			"isActive": true
// // 		},
// // 		"type": "adult",
// // 		"product": [{
// // 			"clientCode": "Client1",
// // 			"productCode": "11119",
// // 			"productVersion": "1",
// // 			"SACCode": 1,
// // 			"SACCounter": "0",
// // 			"productType": "1",
// // 			"productName": "Optima Restore Individual 1 Year",
// // 			"productLine": "9",
// // 			"productGroup": "1",
// // 			"CBAmount": "0",
// // 			"sumAssured": "500000",
// // 			"sourceCode": "Portal",
// // 			"paymentFrequeny": "1",
// // 			"paymentMode": "14"
// // 		}],
// // 		"medical": [{
// // 			"code": "MQ-24",
// // 			"text": "Diabetes",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-25",
// // 			"text": "Thyroid Disorder",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-26",
// // 			"text": "Nervous disorder, fits, mental condition",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-27",
// // 			"text": "Heart & Circulatory disorders",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-28",
// // 			"text": "Respiratory disorder",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-29",
// // 			"text": "Disorders of the stomach including intestine, Kidney, Prostate",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-30",
// // 			"text": "Disorder of Spine and Joints",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-31",
// // 			"text": "Tumour or Cancer",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-32",
// // 			"text": "Any ongoing diseases or ailment requiring surgical or medical treatment",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-33",
// // 			"text": "Have you or any other member proposed to be insured under this policy sought medical advice or undergone any treatment medical or surgical in past 5 years due to any of the diseases/conditions listed above or otherwise or attended follow up for any disease / condition / ailment/ injury / addiction (except for infrequent common illness for example fever, common cold, loose motions, cough and cold, headaches, acidity?",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-34",
// // 			"text": "Is any of the insured pregnant? If yes please mention the expected date of delivery",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}],
// // 		"lifestyle": [{
// // 			"code": "LQ-01",
// // 			"text": "Liquor",
// // 			"heading": "No. of 30 ml pegs of hard liquor",
// // 			"answer": "0",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "LQ-03",
// // 			"text": "Beer",
// // 			"heading": "No. of bottles of beer",
// // 			"answer": "0",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "LQ-02",
// // 			"text": "Wine",
// // 			"heading": "No. of glass of wines",
// // 			"answer": "0",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "LQ-04",
// // 			"text": "Smoke",
// // 			"heading": "No. of Cigarette/bidi sticks",
// // 			"answer": "0",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "LQ-05",
// // 			"text": "Pan Masala/Gutkha",
// // 			"heading": "No. of pouches",
// // 			"answer": "0",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "LQ-06",
// // 			"text": "Others",
// // 			"answer": "0",
// // 			"subQuestions": []
// // 		}],
// // 		"isAgeBandChange": "false"
// // 	}],
// // 	"isProposerCovered": "ture",
// // 	"isMedicalVisited": true,
// // 	"isLifestyleVisited": true,
// // 	"payment": {},
// // 	"leadInfoDetail": {
// // 		"leadNumber": "AMHI020419000385",
// // 		"typeOfCase": "STP"
// // 	},
// // 	"status": {
// // 		"leadStatus": {
// // 			"code": "leadStatus-05",
// // 			"status": "AV lead in Progress"
// // 		}
// // 	},
// // 	"premiumDetail": {
// // 		"basePremium": {}
// // 	},
// // 	"isQuote": "false"
// // }

// //mock data for airtel ican

// // {
// // 	"partnerId": "5c6a8547f9db39453b992aa7",
// // 	"partnerProductId": "5c6a8941f9db39453b992ae6",
// // 	"currentPage": {
// // 		"url": "/app/lead/proposer",
// // 		"header": "proposer",
// // 		"subHeader": 6
// // 	},
// // 	"general": {
// // 		"AMHILocation": {
// // 			"updatedAt": "2019-02-18T10:15:10.873Z",
// // 			"createdAt": "2019-02-18T10:15:10.873Z",
// // 			"partnerId": "5c6a8547f9db39453b992aa7",
// // 			"branchName": "Gurgaon",
// // 			"branchCode": "900001",
// // 			"isActive": true,
// // 			"channels": ["5c6a8575f9db39453b992aa8"],
// // 			"id": "5c6a85aef9db39453b992aa9"
// // 		},
// // 		"partnerLocation": {
// // 			"updatedAt": "2019-02-18T10:15:10.873Z",
// // 			"createdAt": "2019-02-18T10:15:10.873Z",
// // 			"partnerId": "5c6a8547f9db39453b992aa7",
// // 			"branchName": "Gurgaon",
// // 			"branchCode": "900001",
// // 			"isActive": true,
// // 			"channels": ["5c6a8575f9db39453b992aa8"],
// // 			"id": "5c6a85aef9db39453b992aa9"
// // 		},
// // 		"BDRCode": "TSE6",
// // 		"teamCode": "TL6",
// // 		"SMCode": "SM6",
// // 		"subChannel": {
// // 			"code": "VRM",
// // 			"name": "VRM"
// // 		},
// // 		"IMDCode": "IMD6",
// // 		"campaign1": "No",
// // 		"campaign2": "No",
// // 		"AVCode": "Airtel@airtel",
// // 		"SPCode": "SP6"
// // 	},
// // 	"account": {
// // 		"bankVerifierId": "Airtel"
// // 	},
// // 	"product": {
// // 		"product": "5c66a70173b4a40e7fba071a",
// // 		"typeOfBusiness": {
// // 			"id": "5bbb5a7b9c4c8341ebe415bf",
// // 			"type": "New Business",
// // 			"code": "BT-01",
// // 			"isActive": true,
// // 			"updatedBy": {}
// // 		},
// // 		"productName": "iCan",
// // 		"coverType": "individual",
// // 		"totalMember": 1,
// // 		"totalMemberValue": 1,
// // 		"policyTenure": 1,
// // 		"variant": "Enhance Standard"
// // 	},
// // 	"proposer": {
// // 		"title": {
// // 			"id": "5c665fb237feb26f11199d77",
// // 			"title": "Mr.",
// // 			"code": "MR",
// // 			"isActive": true
// // 		},
// // 		"firstName": "Deepak",
// // 		"lastName": "Kumar",
// // 		"mobileNumber": "8920643105",
// // 		"email": "sdfghjjkk@dfghjk.in",
// // 		"dateOfBirth": "19/04/1991",
// // 		"gender": {
// // 			"code": "1",
// // 			"gender": "Male"
// // 		},
// // 		"IdType": {
// // 			"id": "5c665d6d37feb26f11194d4e",
// // 			"code": "IDNO1",
// // 			"description": "Passport",
// // 			"isActive": true
// // 		},
// // 		"IdNumber": "xcfghjk",
// // 		"annualIncome": {
// // 			"_id": "5c665d7d37feb26f11194d53",
// // 			"annualIncome": "Less than 2.5 lac",
// // 			"code": "249999",
// // 			"isActive": true
// // 		},
// // 		"maritalStatus": {
// // 			"maritalStatus": "Single",
// // 			"code": "2",
// // 			"isActive": true,
// // 			"id": "5c665e8237feb26f11195093"
// // 		}
// // 	},
// // 	"address": {
// // 		"ccAddress": "No",
// // 		"address1": "dxfghjk",
// // 		"country": {
// // 			"code": "IN",
// // 			"country": "India"
// // 		},
// // 		"state": {
// // 			"state": "DELHI",
// // 			"code": "010",
// // 			"stateCode": "010"
// // 		},
// // 		"city": {
// // 			"city": "New Delhi",
// // 			"code": "832",
// // 			"stateCode": "010"
// // 		},
// // 		"district": {
// // 			"districtName": "Central Delhi",
// // 			"code": "122",
// // 			"stateCode": "010"
// // 		},
// // 		"pinCode": "110002",
// // 		"timeToContact": {}
// // 	},
// // 	"nominee": {
// // 		"title": {
// // 			"id": "5c665fb237feb26f11199d77",
// // 			"title": "Mr.",
// // 			"code": "MR",
// // 			"isActive": true
// // 		},
// // 		"fullName": "fcghj",
// // 		"relationshipWithProposer": {
// // 			"id": "5c665f4437feb26f11199d23",
// // 			"code": "6",
// // 			"relationship": "Brother",
// // 			"isActive": true
// // 		},
// // 		"address": "dxfghjk, Central Delhi, New Delhi, DELHI, India, 110002",
// // 		"addressSameAsProposer": true
// // 	},
// // 	"member": [{
// // 		"title": {
// // 			"id": "5c665fb237feb26f11199d77",
// // 			"title": "Mr.",
// // 			"code": "MR",
// // 			"isActive": true
// // 		},
// // 		"firstName": "Deepak",
// // 		"lastName": "Kumar",
// // 		"dateOfBirth": "19/04/1991",
// // 		"age": 27,
// // 		"gender": {
// // 			"code": "1",
// // 			"gender": "Male"
// // 		},
// // 		"relationshipWithProposer": {
// // 			"code": "1",
// // 			"relationship": "Self"
// // 		},
// // 		"height": {
// // 			"feet": "5",
// // 			"inch": "9"
// // 		},
// // 		"weight": "65",
// // 		"smokerFlag": "false",
// // 		"occupation": {
// // 			"code": "303601",
// // 			"occupationName": "ACCOUNTANT",
// // 			"occupationClass": "PA_CL_1",
// // 			"occupationClassCode": "613505",
// // 			"isActive": true,
// // 			"id": "5c665eb237feb26f11195164"
// // 		},
// // 		"baseSumInsured": {
// // 			"value": "5",
// // 			"code": "BSI-02",
// // 			"conversion": "500000"
// // 		},
// // 		"maritalStatus": {
// // 			"maritalStatus": "Single",
// // 			"code": "2",
// // 			"isActive": true,
// // 			"id": "5c665e8237feb26f11195093"
// // 		},
// // 		"annualIncome": {
// // 			"_id": "5c665d7d37feb26f11194d53",
// // 			"annualIncome": "Less than 2.5 lac",
// // 			"code": "249999",
// // 			"isActive": true
// // 		},
// // 		"type": "adult",
// // 		"product": [{
// // 			"clientCode": "Client1",
// // 			"productCode": "45011",
// // 			"productVersion": "1",
// // 			"SACCode": 1,
// // 			"SACCounter": "0",
// // 			"productType": "1",
// // 			"productName": "iCan Individual 1 Year",
// // 			"productLine": "2",
// // 			"productGroup": "1",
// // 			"CBAmount": "0",
// // 			"sumAssured": "500000",
// // 			"sourceCode": "Portal",
// // 			"paymentFrequeny": "1",
// // 			"paymentMode": "14"
// // 		}],
// // 		"medical": [{
// // 			"code": "MQ-18",
// // 			"text": "Have you ever been diagnosed with any form of cancer or tumour (includes Lymphoma, Leukaemia, and Sarcoma)?",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-19",
// // 			"text": "Have you ever been suspected to have or investigated for cancer or have been under follow up for cancer or a condition that doctors suspected may become cancerous?",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-20",
// // 			"text": "Have you ever been diagnosed with Human Immunodeficiency virus (HIV), Human papillomavirus infection virus (HPV), Epstein–Barr virus (EBV), Hepatitis B or Hepatitis C",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-21",
// // 			"text": "Have you ever experienced weight loss of more than 5 Kilos over 3 months.",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-22",
// // 			"text": "Did you ever had blood in stool or any bleeding from any other opening for more than 5 days OR fits, Persistent headache or cough in the last 2 years.",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "MQ-23",
// // 			"text": "Has any of your Parents or Siblings or more than one of your parent’s siblings (including your parents) been diagnosed with cancer?",
// // 			"answer": "No",
// // 			"subQuestions": []
// // 		}],
// // 		"lifestyle": [{
// // 			"code": "LQ-07",
// // 			"text": "Does your occupation expose you to radiation, corrosive substances, harmful chemicals, mining, asbestos or explosives?",
// // 			"subQuestions": []
// // 		}, {
// // 			"code": "LQ-08",
// // 			"text": "Have you used tobacco in any form in the last one year? E.g Smoked Beedi , cigarette, Cigar, Cheroot, sheesha ?",
// // 			"subQuestions": [{
// // 				"text": "Number of Units (1 Unit = 1 Beedi/Cigarette/Cigar/Cheeroot/Sheesha per day)"
// // 			}, {
// // 				"text": "Consuming since age of"
// // 			}]
// // 		}, {
// // 			"code": "LQ-09",
// // 			"text": "Have you used chewing tobacco in the last one year? (Pan/Gutkha); Snuff etc",
// // 			"subQuestions": [{
// // 				"text": "Number of Units (1 Unit = No. of Masala/Gutkha Pouches per day)"
// // 			}, {
// // 				"text": "Consuming since age of"
// // 			}]
// // 		}, {
// // 			"code": "LQ-10",
// // 			"text": "Do you consume alcohol, or any narcotic/ habit forming/recreational drug? ",
// // 			"subQuestions": [{
// // 				"text": "Number of Units (1 Unit = 30 ml of hard liquor; 150ml of wine; 330ml of beer, per week)"
// // 			}, {
// // 				"text": "Consuming since age of"
// // 			}]
// // 		}],
// // 		"isAgeBandChange": "false"
// // 	}],
// // 	"isProposerCovered": "ture",
// // 	"isMedicalVisited": true,
// // 	"isLifestyleVisited": true,
// // 	"payment": {},
// // 	"leadInfoDetail": {
// // 		"leadNumber": "AMHI020419000384",
// // 		"typeOfCase": "STP"
// // 	},
// // 	"status": {
// // 		"leadStatus": {
// // 			"code": "leadStatus-05",
// // 			"status": "AV lead in Progress"
// // 		}
// // 	},
// // 	"premiumDetail": {
// // 		"basePremium": {}
// // 	},
// // 	"isQuote": "false"
// // }








































































let medicalJson = {
    "medical_question": [{
        "question_id": 2,
        "question_desc": "Have you undergone any surgery OR hospitalization for more than 10 days at a time in the past OR Are you awaiting any treatment or surgery that you have been advised?",
        "sub_question": "Y"
    },
    {
        "question_id": 3,
        "question_desc": "Have you been consulting a doctor regularly for any disease or complaint OR been under any medication regularly for more than 2 weeks or noticed any growth or tumor in the body?",
        "sub_question": "Y"
    },
    {
        "question_id": 4,
        "question_desc": "Have you experienced pain for more than 7 days in any part of body OR restriction of any movement OR difficulty in swallowing or breathing OR any difficulty in carrying out your daily activities?",
        "sub_question": "Y"
    },
    {
        "question_id": 5,
        "question_desc": "Did you ever have fits, HIV (Human Immune deficiency virus), persistent headache or persistent cough OR blood in stool (frequency) or any bleeding from any other orifice / body opening for more than 5 days?",
        "sub_question": "Y"
    }
    ],
    "medical_details": [{
        "name": "Yash",
        "gender": "M",
        "questionList": [{
            "question_id": 2,
            "isChecked": "Y",
            "subQuestions": [{
                "sub_question_id": "1001",
                "diagnosisDate": "14/06/2017",
                "consultationDate": "07/02/2018",
                "currentStatus": "Under Treatment",
                "complication": "No",
                "dateEpisode": "01/03/2019",
                "treatmentSelect": "Surgical",
                "treatment": false,
                "operated": false,
                "recurrence": false,
                "biopsy": "undefined",
                "dateDiagnosis": "undefined",
                "diseaseType": "undefined",
                "other": "undefined",
            },
            {
                "sub_question_id": "1002",
                "diagnosisDate": "14/06/2017",
                "consultationDate": "07/02/2018",
                "currentStatus": "Under Treatment",
                "complication": "No",
                "dateEpisode": "01/03/2019",
                "treatmentSelect": "Surgical",
                "treatment": false,
                "operated": false,
                "recurrence": false,
                "biopsy": "undefined",
                "dateDiagnosis": "undefined",
                "diseaseType": "undefined",
                "other": "undefined",
            }]
        },
        {
            "question_id": 3,
            "isChecked": "N",
            "subQuestions": []
        },
        {
            "question_id": 4,
            "isChecked": "Y",
            "subQuestions": [{
                "sub_question_id": "1029",
                "diagnosisDate": "18/03/2019",
                "consultationDate": "18/03/2019",
                "currentStatus": "Not Cured",
                "complication": "No",
                "treatmentSelect": "Surgical",
                "treatment": false,
                "operated": false,
                "recurrence": false,
                "biopsy": "undefined",
                "dateDiagnosis": "undefined",
                "diseaseType": "undefined",
                "other": "undefined",
            }]
        },
        {
            "question_id": 5,
            "isChecked": "N",
            "subQuestions": []
        }
        ]
    },
    {
        "name": "Gaurang",
        "gender": "M",
        "questionList": [{
            "question_id": 2,
            "isChecked": "N",
            "subQuestions": []
        },
        {
            "question_id": 3,
            "isChecked": "N",
            "subQuestions": []
        },
        {
            "question_id": 4,
            "isChecked": "N",
            "subQuestions": []
        },
        {
            "question_id": 5,
            "isChecked": "Y",
            "subQuestions": [{
                "sub_question_id": "1038",
                "diagnosisDate": "16/06/2010",
                "consultationDate": "01/03/2019",
                "dateDiagnosis": "2010",
                "currentStatus": "Not Cured",
                "complication": "No",
                "dateEpisode": "01/03/2019",
                "treatment": false,
                "operated": false,
                "recurrence": false,
                "biopsy": "undefined",
                "dateDiagnosis": "undefined",
                "diseaseType": "undefined",
                "other": "undefined"
            }]
        }
        ]
    }
    ]
}


export let mock_data = {
    "isRenewed": false,
    "type": "hni",
    "insureType": "individual",
    "members": 1,
    "age": 20,
    "stay": "1",
    "year": "1",
    "existingHealth": "N",
    "existingSI": "",
    "calculatedSI": 137134.80000000002,
    "requiredSA": 200000,
    "sa": 1500000,
    "rider": 0,
    "deductible": 0,
    "dailyCash": 0,
    "noOfDays": 0,
    "basePremium": 9765,
    "baseDiscount": 0,
    "baseTax": 1757.7,
    "baseGrossPremium": 11522.7,
    "riderPremium": 0,
    "riderDiscount": 0,
    "riderTax": 0,
    "riderGrossPremium": 0,
    "totalPremium": 9765,
    "loadingPremium": 0,
    "totalDiscount": 0,
    "totalTax": 1757.7,
    "totalGrossPremium": 11522.7,
    "ciRiderCheck": false,
    "channelType": "Agency",
    "selectedYear": 1,
    "selectedProduct": {
        "productid": 1,
        "productname": "Easy Health",
        "familyfloater": "Y",
        "twoyr": "Y",
        "rider": "Y",
        "deductible": "N",
        "dailycash": "N",
        "health": "Y",
        "travel": "N",
        "productSubTypeList": [
            "standard",
            "exclusive",
            "premium"
        ],
        "premiumTableName": "regehstdind1",
        "discount": "if(members==2){premium = premium - (premium*0.05);}else if(members>2){premium = premium - (premium*0.1);};return premium;",
        "ageLimit": 0,
        "siLimit": 0,
        "productSubType": "standard",
        "prodCd": "11006",
        "prodVer": 1,
        "prodType": "1",
        "prodLine": "2",
        "prodGroup": 1,
        "sacCode": 1,
        "sacCounter": 1,
        "minMaleAge": 18,
        "maxMaleAge": 65,
        "minFemaleAge": 18,
        "maxFemaleAge": 65,
        "minDependentAge": "91d",
        "maxDependentAge": 25,
        "saList": [
            100000,
            200000,
            300000,
            400000,
            500000,
            750000,
            1000000,
            1500000
        ],
        "dcList": [

        ],
        "ndList": [

        ],
        "ciList": [

        ],
        "dedList": [

        ],
        "ciRider": "Y",
        "ciRiderCheck": false
    },
    "appId": "RJ4NZDI9QP",
    "nachVisiteNotCompleted": false,
    "nachNotVisited": true,
    "memberDetailsList": [
        {
            "title": "MR",
            "name": "Hc",
            "dob": "16/05/1999",
            "age": 20,
            "smoke": "false",
            "smokeDeclaration": false,
            "panGutakha": "false",
            "panGutakhaDeclaration": false,
            "drinks": "false",
            "drinksDeclaration": false,
            "maritalStatus": "1",
            "memberContact": "7503909150",
            "relationship": "99",
            "photo": "profile-1558003183971",
            "isPhotoSelected": false,
            "gender": "M",
            "weight": "55",
            "heightFT": "5",
            "heightIN": "5",
            "heightInCm": "",
            "occupation": "400001",
            "annualIncome": 500000,
            "sa": 1500000,
            "rider": 0,
            "deductible": 0,
            "dailyCash": 0,
            "noOfDays": 0,
            "basePremium": "9765",
            "baseDiscount": 0,
            "baseTax": 1757.7,
            "baseGrossPremium": 11522.7,
            "riderPremium": 0,
            "riderDiscount": 0,
            "riderTax": 0,
            "riderGrossPremium": 0,
            "totalPremium": 9765,
            "totalDiscount": 0,
            "totalTax": 1757.7,
            "newBenefit": 0,
            "nbVersion": 0,
            "totalGrossPremium": 11522.7,
            "medicalAnswer": "false",
            "displayName": "Member 1",
            "src": "",
            "productId": 1,
            "productSubType": "standard",
            "paBasePremium": 0,
            "paBaseDiscount": 0,
            "paBaseTax": 0,
            "paBaseGrossPremium": 0,
            "rejectSmokeFlagiCan": false,
            "paSI1": 0,
            "paSI2": 0
        }
    ],
    "currentMemberNo": 0,
    "proposer": {
        "title": "MR",
        "name": "Hc",
        "dob": "16/05/1999",
        "age": 20,
        "contact": "7503909150",
        "landline": "",
        "email": "a@gmail.com",
        "maritalStatus": "1",
        "address1": "Hgcf",
        "address2": "",
        "address3": "",
        "state": "010",
        "zipcode": "110041",
        "zone": "NCR",
        "portabilityCase": "false",
        "profession": "7",
        "photo": "profile-1558003184129",
        "isPhotoSelected": false,
        "gstinNumber": "",
        "ipaOptCase": false,
        "city": "768",
        "cityName": "Bawana",
        "districtCode": "130",
        "district": "West Delhi",
        "nationality": "Indian"
    },
    "baseProductFlag": true,
    "pASubType": "PA-STD",
    "region": "rest",
    "pincodeExist": true,
    "eRepository": {
        "selectedOption": "1",
        "aadhar": "",
        "pan": ""
    },
    "geographyRistrictFlag": "0",
    "allRistrictFlag": "0",
    "genericDocument": [
        {
            "doc": "0",
            "docName": "",
            "uploadFile": "",
            "member": "Select Member",
            "docType": "0",
            "memberProposer": "0",
            "name": "SELECT DOCUMENT",
            "src": "",
            "path": "",
            "getDocExt": ""
        }
    ],
    "upCrossFlag": false,
    "nomineeDetails": {
        "nomineeTitle": "MR",
        "nomineeName": "Gc",
        "nomineeRelation": 6,
        "nomineeAddress": "Hgcf   Delhi 110041",
        "appointeeTitle": "",
        "appointeeName": "",
        "appointeeAdd": "",
        "appointeeRelation": "",
        "isNomineeMinor": false
    },
    "policyData": [
        {
            "policyNumber": ""
        }
    ],
    "medical": [
        {
            "name": "Hc",
            "gender": "M",
            "medicalSubStandardDecision": "N",
            "questionList": [
                {
                    "question_id": 2,
                    "isChecked": "N",
                    "mqField": "",
                    "subQuestions": [

                    ]
                },
                {
                    "question_id": 3,
                    "isChecked": "N",
                    "mqField": "",
                    "subQuestions": [

                    ]
                },
                {
                    "question_id": 4,
                    "isChecked": "N",
                    "mqField": "",
                    "subQuestions": [

                    ]
                },
                {
                    "question_id": 5,
                    "isChecked": "N",
                    "mqField": "",
                    "subQuestions": [

                    ]
                }
            ]
        }
    ],
    "medicalDocVisited": false,
    "totalGrossPremiumAll": 11522.7,
    "renPropCreated": true,
    "payment": {
        "method": "online",
        "chequeNo": "",
        "bank": "",
        "bankCode": "",
        "date": "16/05/2019"
    },
    "createdDate": 1558003410656,
    "relValue": "self",
    "neftDetails": {
        "beneficaryName": "",
        "accNo": "",
        "bankName": "",
        "branchName": "",
        "ifscCode": "",
        "micrCode": ""
    }
    // "medicalJson": medicalJson,
};


