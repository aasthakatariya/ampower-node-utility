const readFilePromise = require('fs-readfile-promise');
import handlebars from 'handlebars';
import path from 'path';
import * as util from 'util';
import { createPdf } from '../../services/pdf';
import moment from 'moment'
const base64 = require('base64topdf');
var merge = require('easy-pdf-merge');
let fs = require('fs');
import {devLog} from "../../services/utils";
import { promisify } from 'util';
import { master, proposalQuestions } from './constant.js';
import { intToRoman, medicalJson, professionMaster, relationshipMaster, maritalStatusMaster, stateMaster, medicalQuestionMaster, occupations, relationshipToPayorMaster } from './constant-master.js';
import { range, romanize, createSubQuestions}  from './utils';
let APPLICATION_ID = "";
fs.unlink = promisify(fs.unlink);

handlebars.registerHelper("inputIdNo", function (id) {
    let val = ' ';
    if (id) {
        val = id
    }
    return val;
});


handlebars.registerHelper("inc", function (value, options) {
    try {
        return parseInt(value) + 1;
    } catch (err) {
        devLog(err.message, "amp", "inc helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("lengthOf", function (value, options) {
    try{
        if (Array.isArray(value)) {
            return value.length;
        }
        return 0;
    } catch (err) {
        devLog(err.message, "amp", "lengthOf helper", APPLICATION_ID);
        return 0;
    }
    
});
handlebars.registerHelper("tableProposal", function (value) {
    try {
        let checkBoxText = (val) => {
            let html = `Y&nbsp;<input type="checkbox" id="checkbox1" name="Driving License" value="Driving License"`
            if (val && val.toString() == "true") {
                html = html + ` checked `
            }
            html = html + `><label for="checkbox1"></label>/N&nbsp;<input type="checkbox" id="checkbox1" name="Driving License" value="Driving License"`
            if (val && val.toString() == "false") {
                html = html + ` checked `
            }
            html = html + `><label for="checkbox1"></label>`;
            return html;
        }
        let checkBoxTextTD6 = (val) => {
            return `<td style="width:8%;">` + checkBoxText(val[1]) + `</td>` + `<td style="width:8%;">` + checkBoxText(val[2]) + `</td>` + `<td style="width:8%;">` + checkBoxText(val[3]) + `</td>` + `<td style="width:8%;">` + checkBoxText(val[4]) + `</td>` + `<td style="width:8%;">` + checkBoxText(val[5]) + `</td>` + `<td style="width:8%;">` + checkBoxText(val[6]) + `</td>`;
        };
        let html = ``;
        if (value && Object.keys(value.arr).length > 0) {
            Object.keys(value.arr).forEach((entry, index) => {
                html = html + `<tr >`;
                if (index == 0) {
                    html = html + ` <td style="width:17%;" rowspan="${Object.keys(value.arr).length + 1}" class="centerText"> ${value.desc}</td>`
                }
                html = html + `<td class="verticalCenter" style="width:35%;">${value.arr[entry].text}</td>${checkBoxTextTD6(value.arr[entry])}`
                html = html + `</tr>`
            });

        }

        // html = html + `<tr style="height:60px;"><td>Other Medical Condition</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>`
        return html;

    } catch (err) {
        devLog(err.message, "amp", "tableProposal helper", APPLICATION_ID);
        return "";
    }
    
});
handlebars.registerHelper("checkedIf2", function (value, matcher) {
    try {
console.log("checkedif2 helper")
        let checkedValue = "";
        value = value + "";
        matcher = matcher + "";
        if (value != 'undefined' && matcher != 'undefined' && value.toString().replace(/ +/g, "").toLowerCase().trim() == matcher.toString().replace(/ +/g, "").toLowerCase().trim()) {
            checkedValue = "checked";
        }
console.log("returning checkedif2 helper with",checkedValue)
        return checkedValue;
    } catch (err) {
console.log("error in checkedif2 helper", err)
        devLog(err.message, "amp", "checkedIf2 helper", APPLICATION_ID);
        return "";
    }
    
});


handlebars.registerHelper("checkIfHDCRider", function (value1, value2, matcher) {
    try {
        
        let checkedValue = "";
        if(value1)
        {
        value2 = value2 + "";
        matcher = matcher + "";
        if (value2 != 'undefined' && matcher != 'undefined' && value2.toString().replace(/ +/g, "").toLowerCase().trim() == matcher.toString().replace(/ +/g, "").toLowerCase().trim()) {
            checkedValue = "checked";
        }
        return checkedValue;
    }
    return checkedValue;
    } catch (err) {
        devLog(err.message, "amp", "checkIfHDCRider helper", APPLICATION_ID);
        return "";
    }
    
});



handlebars.registerHelper("checkedIfNot", function (value, matcher) {
    try {
	console.log("checked if not helper")
        let checkedValue = "";
        if (value && matcher && value.toString().replace(/ +/g, "").toLowerCase().trim() != matcher.toString().replace(/ +/g, "").toLowerCase().trim()) {
            checkedValue = "checked";
        }
	console.log("returning if not helper ",checkedValue);
        return checkedValue;
    } catch (err) {
	console.log("error in checked if not helper",err)
        devLog(err.message, "amp", "checkedIfNot helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("checkedIfOptima4", function (value, index, matcher) {
    try {
        let checkedValue = "";
        if (value[index] && matcher && value[index].medicalAnswer && value[index].medicalAnswer.toLowerCase().trim() == matcher.toLowerCase().trim()) {
            checkedValue = "checked";
        }
        return checkedValue;
    } catch (err) {
        devLog(err.message, "amp", "checkedIfOptima4 helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("checkedIfAlcohol", function (value, index, matcher) {
    try {
        let checkedValue = "";
        if (value[index] && matcher && value[index].drinks && value[index].drinks.toLowerCase().trim() == matcher.toLowerCase().trim()) {
            checkedValue = "checked";
        }
        return checkedValue;
    } catch (err) {
        devLog(err.message, "amp", "checkedIfAlcohol helper", APPLICATION_ID);
        return "";
    }
})

handlebars.registerHelper("ifPaymentOther", function (value) {
    try {
        let paymentName = value.method.toString().replace(/ +/g, "").toLowerCase().trim();
        const paymentArr = ['cash', 'cheque', 'debitcard', 'creditcard'];
        if (paymentArr.includes(paymentName)) {
            paymentName = "";
        }
        return paymentName;
    } catch (err) {
        devLog(err.message, "amp", "ifPaymentOther helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("checkIfExistsAndReturnValue", function (value, subvalue) {
    try {
        if (value) {
            return value[subvalue];
        }
        return '';
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "checkIfExistsAndReturnValue helper", APPLICATION_ID);
        return "";
    }
});


handlebars.registerHelper("checkIfIPARider", function (value1,subvalue1, value2) {
    try {
        //console.log("++++++++++++++++++++", value1, subvalue1, value2);
       if(value1)
       {
        //console.log("++++++++++++++++++++", value1);

            if(value1[subvalue1]=='99')
            {
                //console.log("++++++++++++++++++++",subvalue1, value2);

                if (value2) {
                    //console.log("++++++++++++++++++++", value2);

                    return 'Y';
                }
                return 'N';
            }return 'N';
        }
        return "";
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "checkIfIPARider helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("checkIfProtectorRider", function (value) {
    try {
        if (value) {
            return 'Y';
        }
        return 'N';
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "checkIfProtectorRider helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("medicalJsonName", function (code) {
    try {
        let medical = medicalJson.filter(arr => { if (arr.sub_question_id == code) return arr; })
        return medical ? medical[0] ? medical[0].value ?  medical[0].value : "" : "" : "";
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "medicalJsonName helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("professionName", function (code) {
    try {
        code = parseInt(code);
        return professionMaster[code];
    } catch (err) {
        devLog(err.message, "amp", "professionName helper", APPLICATION_ID);
        return "";
    }
    
});

handlebars.registerHelper("maritalStatusName", function (code) {
    try {
        code = parseInt(code);
        return maritalStatusMaster[code];
    } catch (err) {
        devLog(err.message, "amp", "maritalStatusName helper", APPLICATION_ID);
        return "";
    } 
});

handlebars.registerHelper("stateName", function (code) {
    try {
        return stateNameMap(code);
    } catch (err) {
        devLog(err.message, "amp", "stateName helper", APPLICATION_ID);
        return "";
    }
   
});

const stateNameMap =  (code) =>  {
    try {
        code = parseInt(code);
        return stateMaster[code];    
    } catch (err) {
        devLog(err.message, "amp", "stateNameMap", APPLICATION_ID);
        return "";
    }
    
}


handlebars.registerHelper("longToDate", function (value) {
    try {
        if (!value) return "";
        // console.log("---------------------------->",typeof(value), value);
        if (typeof (value) == "string" && value.split('/').length == 3) {
            return value;
        }
        if (typeof (value) == "number") {
            let date = moment(value).format('DD/MM/YYYY')
            return date;
        }
        return "";
    } catch (err) {
        devLog(err.message, "amp", "longToDate helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper("relCodeToRelationship", function (value) {
    try {
        let relation = relationshipMaster[value];
        return relation ? relation : "";
    } catch (err) {
        devLog(err.message, "amp", "relCodeToRelationship helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper("relCodeToRelationshipToPayor", function (value) {
    try {
        let relation = relationshipToPayorMaster[value];
        return relation ? relation : "";
    } catch (err) {
        devLog(err.message, "amp", "relCodeToRelationshipToPayor helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper("titleCase", function (value) {
    try {
        return toTitleCase(value);
    } catch (err) {
        devLog(err.message, "amp", "titleCase helper", APPLICATION_ID);
        return value ? value : "";
    }
})

handlebars.registerHelper("toDouble", function (value) {
    try {
        let amount = parseFloat(value);
        return amount.toFixed(2);
    } catch (err) {
        devLog(err.message, "amp", "toDouble helper", APPLICATION_ID);
        return 0;
    }
   
})

handlebars.registerHelper("checkIfMemberDetails", function (member, index) {
    try {
        let checked = "";
        if (!member[index]) {
            return checked;
        }
        let ifSmoke = member[index].smoke == 'true' ? true : false;
        let ifDrinks = member[index].drinks == 'true' ? true : false;
        if (ifSmoke || ifDrinks || member[index].alcoholOpt || member[index].tobaccoOpt) {
            checked = "checked";
        }
        return checked;
    } catch (err) {
        devLog(err.message, "amp", "checkIfMemberDetails helper", APPLICATION_ID);
        return "";        
    }
})

handlebars.registerHelper("printValue", (value)  =>{
    try {
        return value;
    } catch (err) {
        devLog(err.message, "amp", "printValue helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper('ifIndex', function (memberDetailsList, index) {
    try {
        if (Object.keys(memberDetailsList[index]).length === 0 && memberDetailsList[index].constructor === Object) {
            return "";
        }
        return "checked";
    } catch (err) {
        devLog(err.message, "amp", "ifIndex helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("getOccupationFromCode", function (code, proposer, relationship){
    try {
        if (!code) return ""; 
        let occupation = occupations.find(ele => ele.occupCd == code);
        if (occupation) {
            
            if (proposer && Object.keys(proposer).length > 0 && proposer.ipaOptCase && proposer.ipaCheck) {
                if (proposer.ipaCheck == "ipaPh") {
                    return relationship == 99 ? occupation.occupVal : "NA";
                } 
                if (proposer.ipaCheck == "ipaAllmem"){
                    return relationship == 99 || relationship == 14 || relationship == 15 ? occupation.occupVal : "NA";
                }

            }
        }
        return "NA";
    } catch (err) {
        devLog(err.message, "amp", "getOccupationFromCode helper", APPLICATION_ID);
        return "";
    }
})

handlebars.registerHelper("checkIfNotMemberDetails", function (member, index) {
    try {
        let checked = "checked";
        if (!member[index] || Object.keys(member[index]).length == 0) {
            return "";
        }
        let ifSmoke = member[index].smoke == 'true' ? true : false;
        let ifDrinks = member[index].drinks == 'true' ? true : false;
        if (ifSmoke || ifDrinks || member[index].alcoholOpt || member[index].tobaccoOpt) {
            checked = "";
        }
        return checked;
    } catch (err) {
        devLog(err.message, "amp", "checkIfNotMemberDetails helper", APPLICATION_ID);
        return "";
    }
})

handlebars.registerHelper("checkConditionReturnVal", function (variable, matcher, value) {
    try {
        if (!variable || !matcher) {
            return "";
        }
        if (typeof (value) == "number") {
            value = value + "";
        }
        if (variable == matcher) {
            return value;
        }
    } catch (err) {
        devLog(err.message, "amp", "checkConditionReturnVal helper", APPLICATION_ID);
        return "";
    }
})

handlebars.registerHelper("checkedIfOtherProfession", function (value, matcher) {
    try {
        let checkedValue = "";
        if (value && matcher && value.toString().replace(/ +/g, "").toLowerCase().trim() == matcher.toString().replace(/ +/g, "").toLowerCase().trim()) {
            checkedValue = "checked";
        }
        if (value != '1' && value != '2') {
            checkedValue = "checked";
        }
        return checkedValue;
    } catch (err) {
        devLog(err.message, "amp", "checkedIfOtherProfession helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper("criticalIllnessSumInsured", function (ciRiderCheck, sa, rider ,value) {

    try {
        let checked = "";
        if (ciRiderCheck) {
            let sumAssured = parseInt(sa);
            let criticalIllness = parseInt(rider);
            if (isNaN(sumAssured) || isNaN(criticalIllness)) {
                return "";
            }
            // if (sa == rider && value == "100") {
            //     return "checked";
            // }

            // if (rider * 2 == sa && value == "50") {
            //     return "checked";
            // }

            if(rider ==  1000000 && value == "100"){
                return "checked";
            }
            if (rider != 1000000 && value == "50") {
                return "checked";
            }
        }
        return "";
    } catch (err) {
        devLog(err.message, "amp", "criticalIllnessSumInsured helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper("lengthOflist", function(list) {
    try {
        if (Array.isArray(list)) {
            return list.filter(ele => ele).length;
        }
        return 0;
    } catch (err) {
        devLog(err.message, "amp", "lengthOflist helper", APPLICATION_ID);
        return 0;
    }
    
})

handlebars.registerHelper("getAddress", function(proposer) {
    try {
        return `${proposer.address1 ? proposer.address1 + "," : ""} ${proposer.address2 ? proposer.address2 + "," : ""} ${proposer.address3 ? proposer.address3 + "," : ""}  ${proposer.district}, ${proposer.cityName}, ${stateNameMap(proposer.state)}, ${proposer.zipcode ? proposer.zipcode : ''}`
    } catch (err) {
        devLog(err.message, "amp", "getAddress helper", APPLICATION_ID);
        return "";
    }
})

handlebars.registerHelper("toDate", function(date) {
    try {
        return date ? moment(date).format("DD-MM-YYYY") : "";
    } catch (err) {
        devLog(err.message, "amp", "toDate helper", APPLICATION_ID);
        return "";
    }
})

handlebars.registerHelper("checkAndReturnValue", function (value, subvalue, condition) {
    try {
        if (value && condition == "individual") {
            return value[subvalue];
        }
        return '';
    } catch (err) {
        devLog(err.message, "amp", "checkAndReturnValue helper", APPLICATION_ID);
        return "";
    }
});

handlebars.registerHelper("height", function(value) {
    try {
        console.log(value.heightFT);
        if (value.heightFT) {
            return `${value.heightFT}ft ${value.heightIN ? value.heightIN : "0"}in`
        }
        return "";
    } catch (err) {
        devLog(err.message, "amp", "height helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper("advisorCode", function(value){
    try {
        if (value.channelType == "D2C" || value.channelType == "Agency") {
            return value.agent ? (value.agent.agentId ? value.agent.agentId : "") : ""
        }

        if (value.channelType == "Partner") {
            return `${value.agent ? (value.agent.agentId ? value.agent.agentId : "") : ""} ${value.agent ? (value.agent.firstName ? value.agent.firstName : "") : ""} ${value.agent ? (value.agent.lastName ? value.agent.lastName : "") : ""}`
        }

        return "";
    } catch (err) {
        devLog(err.message, "amp", "advisorCode helper", APPLICATION_ID);
        return "";
    }
    
})

handlebars.registerHelper('eachPortabilityInfo', function (context, index, options) {
    try {
        if (!context) return "";
        var ret = "";
        let i = 1;
        if (!context[index]) { return "" };
        for (var prop in context[index].portabilityInfo) {
            if (context[index].portabilityInfo[prop] && context[index].portabilityInfo[prop].policyNumber) {
                context[index].portabilityInfo[prop].ind = i++;
                ret = ret + options.fn({ property: prop, value: context[index].portabilityInfo[prop] });
            }
        }
        return ret;
    } catch (err) {
        devLog(err.message, "amp", "eachPortabilityInfo helper", APPLICATION_ID);
        return "";
    }
    
});

handlebars.registerHelper('checkedIfContinuous', function(value) {
    try {
        if (!value) {
            return "";
        }
        if (!value[0].contYrsOfCoverage) { return "" };
        if (typeof (value[0].contYrsOfCoverage) == "number") {
            if (value[0].contYrsOfCoverage > 0) return "checked";
        }
        if (typeof (value[0].contYrsOfCoverage) == "string") {
            let n = parseInt(value[0].contYrsOfCoverage);
            if (!isNaN(n)) {
                if (n > 0) return "checked";
            }
        }
        return "";
    } catch (err) {
        devLog(err.message, "amp", "checkedIfContinuous helper", APPLICATION_ID);
        return "";
    }  
})

handlebars.registerHelper('checkedIfNotContinuous', function (value) {
    try {
        if (!value) {
            return "checked";
        }
        if (!value[0].contYrsOfCoverage) { return "checked" };
        if (typeof (value[0].contYrsOfCoverage) == "number") {
            if (value[0].contYrsOfCoverage == 0) return "checked";
        }
        if (typeof (value[0].contYrsOfCoverage) == "string") {
            let n = parseInt(value[0].contYrsOfCoverage);
            if (!isNaN(n)) {
                if (n == 0) return "checked";
            }
        }
        return "";
    }
    catch (err) {
        devLog(err.message, "amp", "checkedIfNotContinuous helper", APPLICATION_ID);
        return "";
    }  
})

handlebars.registerHelper("continuousCoverage", function(value, index) {
    try {
        if (!value || !value[index]) {
            return "";
        }

        // To find the max of contYrsOfCoverage in portability info - change requirement given by Yash
        // let list = [];
        // list.push(value[index].portabilityInfo[0] ? (value[index].portabilityInfo[0].contYrsOfCoverage ? parseInt(value[index].portabilityInfo[0].contYrsOfCoverage) : 0) : 0);
        // list.push(value[index].portabilityInfo[1] ? (value[index].portabilityInfo[1].contYrsOfCoverage ? parseInt(value[index].portabilityInfo[1].contYrsOfCoverage) : 0) : 0);
        // list.push(value[index].portabilityInfo[2] ? (value[index].portabilityInfo[2].contYrsOfCoverage ? parseInt(value[index].portabilityInfo[2].contYrsOfCoverage) : 0) : 0);
        // list.sort();
        // list.reverse();
        // return list[0];

        return value[index].portabilityInfo[0] && Object.keys(value[index].portabilityInfo[0]).length > 0 ? value[index].portabilityInfo[0].contYrsOfCoverage : "";

    } catch (err) {
        devLog(err.message, "amp", "continuousCoverage helper", APPLICATION_ID);
        return 0;        
    }
    
})

handlebars.registerHelper("ifPortabilityExists", function (name, portability, index, options){


    let fnTrue = options.fn,
        fnFalse = options.inverse;

    try {

        if (name && portability[index]) {
            return fnTrue(this);
        }
        return fnFalse(this)

    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "ifPortabilityExists helper", APPLICATION_ID);
        return fnFalse(this)
    }
});


handlebars.registerHelper("checkIfSubquestions",  function(medical, options) {
    let fnTrue = options.fn,
        fnFalse = options.inverse;
    try {
        console.log("CALLED++++++++++++++++++++++++++++=")
        console.log("-------------",medical);
        let flag = -1;
        if (Array.isArray(medical) && medical.length > 0) {
            medical.forEach(medicalEle => {
                medicalEle.questionList.forEach(ele => {
                    console.log("=++++++++++++++++++", ele);
                    if (ele.subQuestions.length > 0) {
                        console.log("------------------->true")
                        flag = 1;
                    }
                })
            })
           
        } 
        if(flag > 0) {
            return fnTrue(this);
        }else {
            return fnFalse(this);
        }
    } catch(err) {
        console.log(err);
        devLog(err.message, "amp", "checkIfSubquestions helper", APPLICATION_ID);
    }

})

handlebars.registerHelper("checkIfRecurrence", function(value) {
    try {
        if( value == undefined || value == null) return "";
        if (typeof (value) == "boolean") {
            return value ? "Yes" : "No"
        }
        if( typeof(value) == "string") {
            return value;
        }
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "checkIfRecurrence helper", APPLICATION_ID);
        return "";
    }
})

const toTitleCase = (str) => {
    try {
        return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "toTitleCase", APPLICATION_ID);
        return "";
    }
}

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}



// templateFile.foreach(val => {
// let jsonPath = path.join(__dirname, 'templates/ampower', val);
// let tempTemplate = await readFilePromise(jsonPath, { encoding: 'utf-8' });
// htmlTemplate = htmlTemplate + tempTemplate;


const genericPdf = async params => {
    let pdfs = [];
    try {
        let {
            templateFile,
            replacements,
            fileName,
            options,
            directoryName,
        } = params;
        let html;
        // console.log("TEMPLATES---------->",templateFile);
        await asyncForEach(templateFile, async (htmlObj) => {
            // console.log(htmlObj);
            let filename = htmlObj.htmlFileName.split('.')[0];
            let jsonPath = path.join(__dirname, 'templates/ampower', `${filename}.html`);
            let htmlTemplate = await readFilePromise(jsonPath, { encoding: 'utf-8' });
            let template = handlebars.compile(htmlTemplate);
            let pdfOptions = await generatePdfOptions(htmlObj);
            html = template(replacements);
            let pdfFileName = `${filename}_${replacements.appId}.pdf`;
            let pdf = await createPdf(html, pdfOptions, directoryName, pdfFileName);
            pdfs.push(pdf.filename);

        })

        let outputFile = path.resolve(`${__dirname + '/../../pdf_data_ampower/transcripts'}/pdf_${replacements.appId}.pdf`);

        if (pdfs.length > 1) {
            let result = await merged(pdfs, outputFile);
            pdfs.push(outputFile);
            return { filename: outputFile, cleanupFiles: pdfs }
        } else {
            return { filename: pdfs[0], cleanupFiles: pdfs };
        }

    } catch (err) {
        devLog(err.message, "amp", "genericPdf", APPLICATION_ID);        
        let jsonPath = path.join(__dirname, 'templates/ampower', `empty.html`);
        let htmlTemplate = await readFilePromise(jsonPath, { encoding: 'utf-8' });
        let template = handlebars.compile(htmlTemplate);
        let html = template({});
        let directoryName = 'pdf_data_ampower/transcripts';
        let pdf = await createPdf(html, {}, directoryName, "empty.pdf");
        return { filename: pdf.filename, cleanupFiles: [pdf.filename]};
    } finally {
        APPLICATION_ID = "";
    }
};

let merged = (pdfs, outputFile) => {
    try {
        return new Promise((resolve, reject) => {
            merge(pdfs, outputFile, function (err, success) {
                if (err) {
                    // return console.log(err);
                    return reject(err);
                }
                else {
                    return resolve(true);
                }
            })
        })
    } catch (err) {
        devLog(err.message, "amp", "merged", APPLICATION_ID);        
    }
    
}


const generatePdfOptions = async (props) => {
    try {
        let pdfOptions = {};
        if (props.footer) {
            pdfOptions.footer = {
                height: props.footer.height,
                contents: {
                    default: props.footer.contents
                }
            }
        }
        else {
            pdfOptions.footer = {
                height: '1.00cm',
            }
        }
        if (props.border) {
            pdfOptions.border = props.border;
        }
        if (props.paginationOffset) {
            pdfOptions.paginationOffset = props.paginationOffset;
        }
        pdfOptions.header = { height: '3.00cm' };
        return pdfOptions;
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "generatePdfOptions", APPLICATION_ID);        
        throw err;
    }
}



const manipulate_params = async (params) => {
    params.medical = [];
    params.lifestyle = [];
    params.member.forEach(function (entry) {
        entry['mobileNumber'] = params.customerPhoneNumber;
        let data = {};
        let data1 = {};
        entry.medical.forEach(function (medicalInfo) {
            data[medicalInfo['code']] = (medicalInfo['answer'] == 'No') ? 'N' : 'Y';
        })
        params.medical.push(data);
        entry.lifestyle.forEach(function (lifestyle) {
            data1[lifestyle['code']] = lifestyle['answer'];
        })
        params.lifestyle.push(data1);
    })
    return params;
}


const generatetranscriptAmPower = async (params) => {

    try {
        // console.log(params);
        if (!params.templateName) {
            throw 'Please provide template name';
        }

        let templateObj = master.find(ele => ele.templateName == params.templateName);

        if (!templateObj) {
            throw 'No Template Selected!';
        }

        let params1 = await manipulate_params_AmPower(params);

        let pdfOptions = {
            header: {
                // height: '3.00cm'
                height: '2.50cm'
            },
            footer: {
                height: '4.50cm',
                contents: ''
                // contents:
                // '<span style="padding-left:260pt">Commission Paid Report<span style="padding-left:130pt">Page {{page}} of {{pages}}</span>'
            }
        };
        let templateFile = removeTemplate(templateObj.templates, params1);
        // console.log(templateFile);
        APPLICATION_ID = params1.appId;
        let result = await genericPdf({
            templateFile: templateFile,
            replacements: params1,
            options: pdfOptions,
            directoryName: 'pdf_data_ampower/transcripts',
            fileName: 'test' + '.pdf',
        });
        return result;
    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "generatetranscriptAmPower", APPLICATION_ID);        
        throw err;
    }

};

const removeTemplate = (templates, replacements) => {

    try {
        let type = typeof (replacements.proposer.portabilityCase);

        if (type == "string") {
            if (replacements.proposer.portabilityCase.toLowerCase() == "false") {
                templates = templates.filter(ele => { return ele.htmlFileName != 'portability_form.html'; });
            }
        } else {
            if (!replacements.proposer.portabilityCase) {
                templates = templates.filter(ele => { return ele.htmlFileName != 'portability_form.html'; });
            }
        }
        return templates;
    } catch (err) {
        devLog(err.message, "amp", "removeTemplate", APPLICATION_ID);        
        throw err;
    }
    
}

const getProposalQuestions = (params) => {
    try{

        if(!params && !Array.isArray(params) && params.length < 1) {
            return [];
        }

        let proposalQuestion = [];
        params.forEach(element => {
            proposalQuestion.push(createSubQuestions(element.question_id));
        })

        return proposalQuestion;

    } catch (err) {
        console.log(err);
        devLog(err.message, "amp", "getProposalQuestions", APPLICATION_ID);        
        throw err;
    }
}

const manipulate_params_AmPower = async (params) => {
    try {

        let proposal =  getProposalQuestions(params.medical[0].questionList);
        // let proposal = proposalQuestions;
        if (params.medical && params.medical.length > 0) {
            params.medical.forEach((medicalDetail, mainIndex) => {
                // console.log(medicalDetail);
                medicalDetail.questionList.forEach((question, questionIndex) => {
                    // console.log(question);
                    if (proposal[questionIndex]) {
                        Object.keys(proposal[questionIndex].arr).forEach((qu, index) => {
                            proposal[questionIndex].arr[qu][mainIndex + 1] = "false";
                        })
                        question.subQuestions.forEach((subQuestion, subQuestionIndex) => {

                            // console.log(subQuestion.sub_question_id);
                            // console.log(Object.keys(proposal[questionIndex].arr).includes(subQuestion.sub_question_id));
                            if (proposal[questionIndex].arr[subQuestion.sub_question_id]) {
                                proposal[questionIndex].arr[subQuestion.sub_question_id][mainIndex + 1] = "true"
                            }
                        })
                    }
                })
            })
        }


        params.proposal = proposal;
console.log(JSON.stringify(proposal, null, 4));
        if (params.memberDetailsList.length < 6) {
            for (let i = params.memberDetailsList.length; i < 6; i++) {
                params.memberDetailsList.push({})
            }
        }

        let optima4 = [];
        // let original_questionIdList = range(2, 5);
        // let optimaCase_questionIdList = range(71, 90);
        // let allowedQuestions = [].concat(original_questionIdList).concat(optimaCase_questionIdList);
        if (params.medical && params.medical.length > 0) {
            params.medical[0].questionList.forEach((val, index) => {
                // if (allowedQuestions.includes(val.question_id)) {

                    let obj = {
                        incNum: romanize(index + 2),
                        id: val.question_id,
                        text: medicalQuestionMaster[val.question_id],
                        checkBoxVal: []
                    }
                    // console.log("Oooooooooooooooooooooooooo", obj);
                    for (let i = 0; i < 6; i++) {
                        if (!params.medical[i]) {
                            obj.checkBoxVal[i] = ''
                        }
                        else if (!params.medical[i].questionList[index]) {
                            obj.checkBoxVal[i] = ''
                        }
                        else {
                            obj.checkBoxVal[i] = params.medical[i].questionList[index].isChecked
                        }
                    }
                    optima4.push(obj)
                })

            // })
        }
        params.optima4 = optima4;

        params.currentDate = moment().utcOffset("+5:30").format('DD-MM-YYYY');
        params.currentTime = moment().add("5", "hours").add("30", "minutes").format('hh:mm A')
        return params;

    } catch (err) {
        devLog(err.message, "amp", "manipulate_params_AmPower", APPLICATION_ID);        
        throw "This shouldn't have happened";
    }
}




module.exports = { genericPdf, generatetranscriptAmPower }
