import { medicalJson, medicalQuestionMaster } from './constant-master';

const createSubQuestions =  (questionId) => {
    try {

        if (!questionId || isNaN(parseInt(questionId))) {
            return null;
        }
        let question = medicalQuestionMaster[questionId];
        if (!question) return null;

        let questionSubquestions = medicalJson.filter(ele => ele.question_id == parseInt(questionId));
        let subQuestionObj = prepareSubQuestionObj(question, questionSubquestions);
        return subQuestionObj;
    } catch (err) {
        console.log(err);
    }
}
 
const prepareSubQuestionObj = (questionText, subQuestions) => {
    let subQuestionObj = {desc: questionText, arr: {}};
    subQuestions.forEach(element => {
        subQuestionObj.arr[element.sub_question_id] = { text: element.value};
    });
    return subQuestionObj;
}

const range = (start, end) => {
    return Array(end - start + 1).fill().map((_, idx) => start + idx)
}

const romanize = num => {
    var lookup = { m: 1000, cm: 900, d: 500, cd: 400, c: 100, XC: 90, l: 50, xl: 40, x: 10, ix: 9, v: 5, iv: 4, i: 1 }, roman = '', i;
    for (i in lookup) {
        while (num >= lookup[i]) {
            roman += i;
            num -= lookup[i];
        }
    }
    return `${roman}.`;
}




module.exports.createSubQuestions = createSubQuestions;
module.exports.range = range;
module.exports.romanize = romanize;