import path from 'path';
import fs from 'fs';
let pdf = require('html-pdf');
const defaultOptions = {
  //"format": "A4",
  //"orientation": "landscape",
  //"height": "8.27in",
  //"width": "11.69in"
  format: 'A4',
  orientation: 'portrait'
};
const createPdf = async (html, options, dirName, filename) => {
  try {
    options = Object.assign({}, options, defaultOptions);
    if(!dirName){
    dirName = 'pdf_data';
    }
    let dir = path.join(__dirname, "..", "..", dirName);
    console.log("path : ", dir);
    let pathStats = await checkIfDirectoryExists(dir);
    console.log(pathStats);
    if (!pathStats) {
        console.log("Creating dir");
        fs.mkdirSync(dir);
    }
    let jsonPath = path.join(dir, filename);
    let result = await (async () => {
      return new Promise((resolve, reject) => {
        pdf.create(html, options).toFile(jsonPath, (err, result) => {
          if (err) {
            reject(err);
          }
          resolve(result);
        });
      });
    })();
    return result;
  } catch (err) {
    throw err;
  }
};
let checkIfDirectoryExists = async (dir) => {
  return new Promise((res, rej) => {
      try {
          fs.statSync(dir);
          return res(true);
      } catch (error) {
          return res(false);
      }
  })
}

const getFile = async (excelFileName ,folderName) => {
  try {
      let fileName = excelFileName;
      let dir = path.join(__dirname, "..", "..", "pdf_data/" + folderName);
      let jsonPath = path.join(dir, fileName);
      console.log(jsonPath);
      let filePath = fs.readFileSync(jsonPath, 'binary');
      return filePath;
  } catch (err) {
      console.log(err);
      throw err;
  }
}

module.exports = { createPdf ,getFile };
