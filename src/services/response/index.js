import { serviceResponse } from '../utils';
let fs = require('fs');
import { promisify } from 'util';
fs.unlink = promisify(fs.unlink);

export const success = (res, status) => (entity) => {
  if (entity) {
    res.status(status || 200).json(entity)
  }
  return null
}

export const notFound = (res) => (entity) => {
  if (entity) {
    return entity
  }
  res.status(404).end()
  return null
}

export const authorOrAdmin = (res, user, userField) => (entity) => {
  if (entity) {
    const isAdmin = user.role === 'admin'
    const isAuthor = entity[userField] && entity[userField].equals(user.id)
    if (isAuthor || isAdmin) {
      return entity
    }
    res.status(401).end()
  }
  return null
}

export const downloadFileAndCleanup = (res, filePath, cleanupFiles, isExcel) => {
  res.status(200).download(filePath);
  res.on('finish', function () {
    cleanupFiles.forEach(ele => {
      clearFile(ele)
        .then(res => {
          console.log('successfull  delete file');
        })
        .catch(err => {
          console.log('error in deleting file', err);
        });
    })
  });
  return null;
};

const clearFile = async (absolutefileName) => {
  try {
    let deletedFile = await fs.unlink(absolutefileName);
    return null;
  } catch (err) {
    throw err;
  }
}
