import moment from 'moment';

import { ampowerLogger } from "../logger";
const devLog = (object, context, caller, applicationId = "") => {
	console.log("DEVLOG ------------------------------------")
	switch (context) {
		case 's':
			console.log(`\nsuccess\n`)
			console.log(object)
			console.log(`\n`)
			console.log(caller);
			console.log(`\n`);
			console.log('log Time', new Date(), '\n');
			break;
		case 'e':
			console.log(`\nerror\n`)
			console.log(object)
			console.log(`\n`)
			console.log(caller);
			console.log(`\n`);
			console.log(`\n`);
			console.log('log Time', new Date(), '\n');
			break;
		case 'amp':
			console.log(`\nerror amp\n`)
			console.log(applicationId)
			console.log(`\n`)
			console.log(object);
			console.log(`\n`)
			console.log(caller);
			console.log(`\n`);
			console.log(`\n`);
			console.log('log Time', new Date(), '\n');
			//		ampowerLogger.verbose({applicationId, object, caller});
			break;
		case 'default':
			console.log(`\ngeneric\n`)
			console.log(object)
			console.log(`\n`)
			console.log(caller);
			console.log(`\n`);
			console.log(`\n`);
			console.log('log Time', new Date(), '\n');
	}
}

const response = {
	"err": "",
	"res": ""
}

const serviceResponse = (params) => {
	let err = params.err || null;
	let res = params.res || null;
	return { err, res };
}

module.exports.response = response
module.exports.devLog = devLog
module.exports.serviceResponse = serviceResponse
